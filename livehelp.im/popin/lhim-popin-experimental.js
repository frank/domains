var lhimIIFE = (function () {
  'use strict';

  var version = "0.0.2-experimental";

  function createMinimized(){
  	const min = baseDiv();
  	Object.assign(min.style, {
  		zIndex: '20000',
  		cursor: 'move',
  		margin: '0px',
  		padding: '1px',
  		// backgroundColor: 'Red',
  		backgroundImage: 'url(https://livehelp.im/img/logo.svg)',
  		backgroundRepeat: 'no-repeat',
  		backgroundPosition: 'center',
  		backgroundSize: '40px 40px',
  		width: '40px',
  		minHeight: '40px',
  		position: 'fixed',
  		top: 1,
  		left: 1,
  		// verticalAlign: 'middle',
  		// textAlign: 'center',
  	});
  	// min.innerHTML = 'Live<br>Help'
  	return min;
  }


  function createContainer(){
  	const container = baseDiv();
  	Object.assign(container.style, {
  		zIndex: '20001',
  		border: '1px solid purple',
  		margin: '0px',
  		padding: '1px',
  		backgroundColor: 'white',
  		width: '300px',
  		minHeight: '310px',
  		position: 'fixed',
  	});
  	return container;
  }

  function createClose(){
  	const close = baseDiv();
  	Object.assign(close.style, {
  		cursor: 'pointer',
  		display: 'inline-block',
  		position: 'absolute',
  		backgroundColor: '#666666',
  		top: '1px',
  		right: '3px',
  		margin: '0px',
  		padding: '0px',
  		color: '#ff2222',
  		width: '14px',
  		height: '14px',
  		paddingLeft: '2px',
  		paddingTop: '2px',
  		lineHeight: '1.5',
  		fontWeight: 'bold',
  	});
  	close.innerHTML = 'X';
  	return close;
  }

  function createHeader(close){
  	const header = baseDiv();
  	Object.assign(header.style, {
  		cursor: 'move',
  		margin: '0px',
  		padding: '2px',
  		border: '0px solid red',
  		backgroundColor: '#666666',
  		color: 'white',
  		height: '24px',
  		lineHeight: '1.5',
  		verticalAlign: 'middle',
  		textAlign: 'center',
  	});
  	header.innerHTML = 'Livehelp.IM Popin Client';
  	header.appendChild(close);
  	return header;
  }

  function createBody(){
  	const body = baseDiv();
  	Object.assign(body.style, {
  		margin: '0px',
  		padding: '2px',
  		height: '270px',
  		border: '0px solid blue',
  	});
  	return body;
  }

  function createFooter(input, submit){
  	const footer = baseDiv();
  	Object.assign(footer.style,{
  		cursor: 'text',
  		backgroundColor: '#BBBBBB',
  		color: 'black',
  		lineHeight: '1',
  		height: '30px',
  		width: '100%',
  		verticalAlign: 'middle',
  		display: 'flex',
  		flexFlow: 'row nowrap',
  		justifyContent: 'space-between',
  		alignItems: 'center',
  	});

  	footer.appendChild(submit);
  	const inputContainer = baseDiv();
  	Object.assign(inputContainer.style,{
  		display: 'flex-item',
  		height: '30px',
  		verticalAlign: 'middle',
  		textAlign: 'center',
  	});

  	inputContainer.appendChild(input);
  	footer.appendChild(inputContainer);
  	return footer;
  }

  function createInput(){
  	const input = document.createElement('input');
  	commonFormatting(input);
  	Object.assign(input.style,{
  		margin: '0px',
  		padding: '0px',
  		paddingLeft: '5px',
  		border: '1px solid #666666',
  		backgroundColor: '#BBBBBB',
  		height: '28px',
  		width: '250px',
  		fontSize: '.8rem',
  		verticalAlign: 'middle',
  	});
  	input.type='text';
  	input.placeholder = 'Enter message here';
  	return input;
  }

  function createSubmit(){
  	const submit = baseDiv();
  	Object.assign(submit.style,{
  		cursor: 'pointer',
  		display: 'flex-item',
  		margin: '0px',
  		padding: '0px',
  		paddingTop: '7px',
  		border: '0px solid brown',
  		backgroundColor: '#CCCCCC',
  		color: 'black',
  		height: '23px',
  		width: '50px',
  		verticalAlign: 'middle',
  		textAlign: 'center',
  	});
  	submit.innerHTML = 'Send';
  	return submit;
  }

  function commonFormatting(element){
  	Object.assign(element.style, {
  		margin: '0',
  		padding: '0',
  		border: '0',
  		fontFamily: 'sans-serif',
  		fontSize: '1rem',
  		fontWeight: '400',
  		lineHeight: '1',
  	});
  	return element;
  }

  function baseDiv(){
  	const div = document.createElement('div');
  	commonFormatting(div);
  	return div;
  }

  function offsetX(){
  	const px = getComputedStyle(document.body, null).getPropertyValue('padding-left');
  	const mx = getComputedStyle(document.body, null).getPropertyValue('margin-left');
  	return (parseInt(px.toString().substr(0,px.length - 2)) || 0) + (parseInt(mx.toString().substr(0,mx.length - 2)) || 0);
  }

  function offsetY(){
  	const py = getComputedStyle(document.body, null).getPropertyValue('padding-top');
  	const my = getComputedStyle(document.body, null).getPropertyValue('margin-top');
  	return (parseInt(py.toString().substr(0,py.length - 2)) || 0) + (parseInt(my.toString().substr(0,my.length - 2)) || 0);
  }


  function clientWidth(){
  	return document.documentElement.clientWidth;
  }

  function clientHeight(){
  	return document.documentElement.clientHeight; 
  }

  class LHIMPopin extends HTMLElement {
  	constructor(){
  		super();
  		const shadow = this.attachShadow({mode: 'closed'});
  		var baseState = {
  			isDragging: false,
  			isHidden: true,
  			xDiff: 0,
  			yDiff: 0,
  			x: 10000,
  			y: 10000,
  			bodyText: 'Body Text',
  			marginTop: 10,
  			marginBottom: 10,
  			marginLeft: 10,
  			marginRight: 10,
  		};

  		const _container = createContainer();
  		const _close = createClose();
  		const _header = createHeader(_close);
  		const _body = createBody();
  		const _input = createInput();
  		const _submit = createSubmit();
  		const _footer = createFooter(_input, _submit);
  		const term = genDraggable(_container, _header);
  		shadow.appendChild(_container);
  		_container.appendChild(_header);
  		_container.appendChild(_body);
  		_container.appendChild(_footer);
  		_close.addEventListener('click', closeTerm);

  		const _lhim = createMinimized();
  		const lhim = genDraggable(_lhim, _lhim);
  		lhim.state.isHidden = false;
  		shadow.appendChild(_lhim);
  		_lhim.title="I'm Live Help";
  		_lhim.addEventListener('click', openTerm);

  		this.term = term;
  		this.lhim = lhim;
  		document.addEventListener('scroll', render);
  		this.onresize = render;
  		setTimeout(render,1000);

  		function openTerm() {
  			if(lhim.state.stopClick) return;
  			term.state.isHidden = false;
  			render();
  		}

  		function closeTerm() {
  			if(term.state.stopClick) return;
  			term.state.isHidden = true;
  			render();
  		}

  		function render(){
  			lhim.render();
  			term.render();
  		}

  		function genDraggable(element, handle){
  			const state = Object.assign({}, baseState);
  			handle.addEventListener('mousedown', onMouseDown);
  			document.addEventListener('mousemove', onMouseMove);
  			document.addEventListener('mouseup', onMouseUp);
  			element.style.visibility = state.isHidden?'hidden':'';

  			function render() {
  				state.x = clampX(state.x);
  				state.y = clampY(state.y);
  				element.style.visibility = state.isHidden?'hidden':'';
  				element.style.transform = 'translate(' + state.x + 'px, ' + state.y + 'px)';
  			}
  			function onMouseMove(e) {
  				if (state.isDragging) {
  					state.x = clampX(e.pageX - state.xDiff);
  					state.y = clampY(e.pageY - state.yDiff);
  					state.stopClick=true;
  					render();
  				}
  			}
  			function onMouseDown(e) {
  				e.preventDefault();
  				state.isDragging=true;
  				state.xDiff = e.pageX - state.x;
  				state.yDiff = e.pageY - state.y;
  			}
  			function onMouseUp(e) {
  				e.preventDefault();
  				state.isDragging=false;
  				setTimeout(()=>state.stopClick=false,100);
  			}
  			function clampX(n) {
  				return Math.min(Math.max(n,  - offsetX() + state.marginLeft), clientWidth() - elementWidth()- offsetX() - 5 - state.marginRight);
  			}
  			function clampY(n) {
  				return Math.min(Math.max(n,  - offsetY() + state.marginTop), clientHeight() - elementHeight() - offsetY() - 7 - state.marginBottom);
  			}
  			function elementWidth(){
  				const computed = getComputedStyle(element, null);
  				return pxToInt(computed.getPropertyValue('width'))
  					+pxToInt(computed.getPropertyValue('marginLeft'))
  					+pxToInt(computed.getPropertyValue('marginRight'))

  			}
  			function elementHeight(){
  				const computed = getComputedStyle(element, null);
  				return pxToInt(computed.getPropertyValue('height'))
  					+pxToInt(computed.getPropertyValue('marginTop'))
  					+pxToInt(computed.getPropertyValue('marginBottom'))
  			}

  			return {state, render, element, handle, onMouseMove, onMouseDown, onMouseUp};
  		}

  		function pxToInt(px){
  			return parseInt(px.toString().match(/\d+/) || '0');
  		}

  		_input.addEventListener('keyup', e=>{
  			if(e.key == 13){
  				state.sendMessage(_input.value);
  				_input.value = '';
  			}
  		});
  	}
  }

  window.customElements.define('lhim-popin', LHIMPopin);

  /* global window, LHIM */

  if(!(window.LHIM && window.LHIM instanceof Object))
  	window.LHIM = {};
  LHIM.version = version;
  LHIM.runMode = version.match(/experimental/)? 'experimental':
  	version.match(/stable/)? 'stable':
  	'default';
  LHIM.Popin = LHIMPopin;

  if(!window.nhm){
  	const nhmScript = document.createElement('script');
  	nhmScript.type = "text/javascript";
  	switch(LHIM.runMode){
  		case 'experimental': {
  			nhmScript.src = 'https://nhm.ie/client/nhm-client-jsm-experimental.js';
  		} break;
  		case 'stable': {
  			nhmScript.src = 'https://nhm.ie/client/nhm-client-jsm-stable.js';
  		} break;
  		default: {
  			nhmScript.src = 'https://nhm.ie/client/nhm-client-jsm.js';
  		} break;
  	}
  	document.body.appendChild( nhmScript );
  }

  if(!moment) document.body.appendChild( Object.assign(
  	document.createElement('script'),
  	{ src: 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js' }
  ));

  if(!sha256) document.body.appendChild( Object.assign(
  	document.createElement('script'),
  	{ src: 'https://cdnjs.cloudflare.com/ajax/libs/js-sha256/0.9.0/sha256.js' }
  ));

  LHIM.popin = new LHIM.Popin();
  document.body.insertBefore(LHIM.popin, document.body.firstChild);

  var index = window.LHIM;

  return index;

}());
