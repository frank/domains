set _Date=%date:~6,4%-%date:~3,2%-%date:~0,2%
set _Time=%time:~0,2%-%time:~3,2%-%time:~6,2%
set _ServerName=nhm-domains
set ext=7z
if not exist \backup\nul md \backup
copy /y "%_ServerName%.%ext%" "\Backup\%_ServerName%-Previous(%_Date%,%_Time%).%ext%"
del %_ServerName%.%ext%
7z a -r %_ServerName%.%ext% .\ -xr!.git -xr!node_modules
echo on
copy /y "%_ServerName%.%ext%" "\Backup\%_ServerName%(%_Date%,%_Time%).%ext%"
move  /y "%_ServerName%.%ext%" \Backup