app.controller('Main', ['$scope', '$location', '$sce', 'Util', function ($scope, $location, $sce, Util) {

	data.viewport = 'admin';

	sys.edit = page => {
		data.page = Util.findRecord(data.pages, 'tag', page);
		if(!data.page) return sys.goto('welcome');
		$location.path(`/edit/${page}`);
		Util.safeApply();
	}

}]);