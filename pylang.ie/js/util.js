app.factory('Util', ['$rootScope', '$location', function ($rootScope, $location) {

	const Util = nhm.Util;
	const Pages = nhm.DAO('pages');
	const Select = nhm.Select;
	const Global = nhm.Global;

	Object.assign($rootScope, {nhm, sys, data, Pages});
	Object.assign(sys, {$rootScope, Pages})

	sys.login = async credentials => {
		await nhm.User.login(credentials);
		location = '/';
	}

	sys.logout = async () => {
		await nhm.User.logout();
		location = '/';
	}

	sys.loadPages = async ()=>{
		const pages = await Pages.reload();
		const isUser = Global.user.type=='user';
		const isAdmin = Global.user.type=='admin';
		Util.setField(pages, 'hide', true);
		Util.setField(pages, 'viewport', 'home');
		data.pages = [
			...Select.defaultPages,
			...((isUser||isAdmin)?Select.accountPages:[]),
			...(isAdmin?Select.adminPages:[]),
			...pages
		];
		data.menus = Util.cloneRecords(Select.menus);
		const defaultMenu = Util.findRecord(data.menus,'tag','default');
		data.pages.forEach(page=>{
			const menu = Util.findRecord(data.menus,'tag',page.menu) || defaultMenu;
			if(!menu.pages) menu.pages = [];
			menu.pages.push(page);
		})
		Util.safeApply();
	};

	sys.goto = page => {
		data.page = Util.findRecord(data.pages, 'tag', page);
		if(!data.page || !(data.page instanceof Object)) return sys.goto('welcome');
		if(data.page.url instanceof String || typeof data.page.url === 'string'){
			let viewport = data.page.viewport;
			const matches = data.page.url.toString().match(/^\/(home|account|admin)/);
			if(matches instanceof Array) [,viewport] = matches;
			if(viewport && data.viewport!=viewport)
				location = (data.page.redirect || `/${viewport}/${page}`);
			else
				$location.path(`/${page}`);
		} else if(data.page.redirect)
			window.open(data.page.redirect);

		Util.safeApply();
	}

	sys.navbar = {
		init: async ()=>{
			await sys.loadPages();
			let [,page] = $location.path().match(/\/(.+)/);
			sys.goto(page);
			Util.safeApply();
		},
	};

	let applyQueued;
	$rootScope.safeApply = Util.safeApply = function(){
		if(!$rootScope.$$phase){
			$rootScope.$apply();
			applyQueued = false;
		}
		else {
			if(!applyQueued) setTimeout(Util.safeApply, 1000);
			applyQueued = true;
		}
	};

	return Util

}]);