app.factory('Util', ['$rootScope', '$location', function ($rootScope, $location) {

	const Util = nhm.Util;
	const Pages = nhm.DAO('pages',{watching: true});
	let watcher = null;
	const Select = nhm.Select;
	const Global = nhm.Global;

	Object.assign($rootScope, {nhm, sys, data, Pages});
	Object.assign(sys, {$rootScope, Pages})

	sys.login = async credentials => {
		await nhm.User.login(credentials);
		location = '/';
	}

	sys.logout = async () => {
		await nhm.User.logout();
		location = '/';
	}

	sys.loadPages = async ()=>{
		const pages = await Pages.reload();
		const isUser = Global.user.type=='user';
		const isAdmin = Global.user.type=='admin';
		Util.setField(pages, 'hide', true);
		Util.setField(pages, 'viewport', 'home');
		data.pages = [
			...((isUser||isAdmin)?Select.accountPages:[]),
			...(isAdmin?Select.adminPages:[]),
			...pages,
			...Select.defaultPages
		];
		data.menus = Util.cloneRecords(Select.menus);
		const defaultMenu = Util.findRecord(data.menus,'tag','default');
		data.pages.forEach(page=>{
			const menu = Util.findRecord(data.menus,'tag',page.menu) || defaultMenu;
			if(!menu.pages) menu.pages = [];
			menu.pages.push(page);
		})
		Util.safeApply();
	};

	sys.goto = async (page, section) => {
		await sys.loadPages();
		data.page = Util.findRecord(data.pages, 'tag', page);
		if(!data.page || !(data.page instanceof Object)) return sys.goto('welcome');
		if(data.page.url instanceof String || typeof data.page.url === 'string'){
			let viewport = data.page.viewport;
			const matches = data.page.url.toString().match(/^\/(home|account|admin)/);
			if(matches instanceof Array) [,viewport] = matches;
			if(viewport && data.viewport!=viewport)
				location = (data.page.redirect || `/${viewport}/${page}`+(section?`/${section}`:''));
			else{
				$location.path(`/${page}`+(section?`/${section}`:''));
			}
		} else if(data.page.redirect)
			window.open(data.page.redirect);
		else if(data.page.content){
			// if(watcher){
			// 	Pages.unwatch(watcher);
			// 	watcher = null;
			// }
			// console.log('Watching');
			// watcher = await Pages.watch('record',rec=>{
			// 	nhm.Data('pages').read({id:rec.id}).then(page=>{
			// 		Object.assign(data.page, page);
			// 		Util.safeApply();
			// 	});
			// })
			Pages.load(data.page.id);
		}

		Util.safeApply();
	}

	nhm.Comms.watch({type:'pages'},async event=>{
		switch(event.method){
			case 'change': case 'update': {
				const page = Util.findRecord(data.pages,'id',event.id);
				if(!page || !page.id) return;
				const rec = await nhm.Data('pages').read({id: page.id});
				if(rec) Object.assign(page, rec);
				Util.safeApply();
			} break;
		}
	})

	sys.navbar = {
		init: async ()=>{
			let [,page,edit] = $location.path().match(/\/([^\/]+)\/?([^\/]+)?/);
			if(page != 'editor')
				sys.goto(page);
			else {
				sys.edit(edit);
			}
			Util.safeApply();
		},
	};

	let applyQueued;
	$rootScope.safeApply = Util.safeApply = function(){
		if(!$rootScope.$$phase){
			$rootScope.$apply();
			applyQueued = false;
		}
		else {
			if(!applyQueued) setTimeout(Util.safeApply, 1000);
			applyQueued = true;
		}
	};

	return Util

}]);