app.controller('Edit', ['$scope', function ($scope) {

	$scope.options = sys.ckeditorOptions;
	sys.editInline = true;

	$scope.init = async ()=>{
	}

	sys.edit = {
		init: async () => {
			if(data.edit && data.edit.id)
				data.edit = await nhm.Data('pages').read({id});
		},
		changed: field=>{
			if(data.edit && data.edit.id)
				nhm.Data('pages').change({field, id: data.edit.id, value: data.edit[field]});
		}
	}

}]);