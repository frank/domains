app.controller('Main', ['$scope', '$location', '$sce', 'Util', function ($scope, $location, $sce, Util) {

	data.viewport = 'admin';

	const Sections = nhm.DAO('sections');

	Object.assign(sys, {Sections})


	data.page = Util.findRecord(data.pages, 'tag', 'editor');

	sys.edit = async page => {
		data.page = Util.findRecord(data.pages, 'tag', 'editor');
		// if(!data.page) return sys.goto('welcome');
		await sys.goto('editor', page);
		data.edit = await sys.Pages.load(page);
		$location.path(`/editor/${page}`);
		Util.safeApply();
	}

}]);