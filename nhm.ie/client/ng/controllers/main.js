app.controller('Main', ['$rootScope', '$scope', '$location', '$route', '$sce', function ($rootScope, $scope, $location, $route, $sce) {

	Object.assign($rootScope, {nhm, sys, data, $sce});

	const Pages = nhm.DAO('pages');

	sys.navbar = {
		init: async ()=>{
			data.pages = await Pages.reload();
			sys.goto(data.pages[0].id)
			nhm.Util.safeApply();
		} 
	};

	sys.goto = async (page)=>{
		data.page = await Pages.load(page);
		data.title = $sce.trustAsHtml(data.page.name);
		data.content = $sce.trustAsHtml(data.page.content);
		$location.path(`/home/${data.page.tag}`);
		nhm.Util.safeApply();
	}

	$rootScope.safeApply = nhm.Util.safeApply = function(){
		if(!$rootScope.$$phase){
			$rootScope.$apply();
			applyQueued = false;
		}
		else {
			if(!applyQueued) setTimeout(nhm.Util.safeApply, 1000);
			applyQueued = true;
		}
	};

}]);