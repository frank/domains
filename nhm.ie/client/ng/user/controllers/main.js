app.controller('Main', ['$rootScope', '$scope', '$location', '$route', '$sce', function ($rootScope, $scope, $location, $route, $sce) {

	Object.assign($rootScope, {nhm, sys, data, $sce});

	const Pages = nhm.DAO('pages');

	sys.navbar = {
		init: async ()=>{
			data.pages = [
				{tag: 'welcome'},
				{tag: 'dashboard'},
			]
			nhm.Util.safeApply();
		} 
	};

	let applyQueued;
	$rootScope.safeApply = nhm.Util.safeApply = function(){
		if(!$rootScope.$$phase){
			$rootScope.$apply();
			applyQueued = false;
		}
		else {
			if(!applyQueued) setTimeout(nhm.Util.safeApply, 1000);
			applyQueued = true;
		}
	};

}]);