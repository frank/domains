'use strict';

var data = {}; // Global data
var sys = {}; // Global system

var app = angular.module('app', ['ngResource','ngRoute']);

/**
 * When the app is fully loaded
 */
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#';

	// Booting up the Angular Web App docs to the scope of the entire document
	angular.bootstrap(document, ['app']);
});

//Setting HTML5 Location Mode
app.config(['$locationProvider','$routeProvider',
	function($locationProvider, $routeProvider) {
		$locationProvider
			.hashPrefix('!')
			.html5Mode(true);

		$routeProvider
			.when('/welcome', {
				controller: 'Main',
				templateUrl: '/blank.html'
			})
			.when('/:page', {
				controller: 'Main',
				templateUrl: function(params){
					console.log('Page:',params.page);
					if(params.page != 'welcome');
					sys.goto(params.page);
					return '/blank.html';
				}
			})

			.otherwise({
				redirectTo: '/welcome'
			});
	}
]);

app.filter('html', ['$sce', $sce => input => $sce.trustAsHtml(input || '')]);
