/* global window */
import Global from './module/global.js'
import Data from './module/data.js'
import DAO from './module/dao.js'
import User from './module/user.js'
import Comms from './module/comms.js'
import Select from './module/select.js'
import Util from './module/util.js'
import Events from './module/events.js'
import Filters from './module/filters.js'
import Log from './module/log.js'

if(!window.nhm) window.nhm = {};
window.fxc = window.nhm;

var mods = {Global, Data, DAO, User, Comms, Select, Util, Events, Filters, Log}
Object.keys(mods).forEach((mod)=>{
  if(!window.fxc[mod]) window.fxc[mod] = mods[mod]
  else Object.assign(window.fxc[mod], mods[mod])
})

export default window.fxc
