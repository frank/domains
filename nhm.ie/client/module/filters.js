/* eslint-disable */

/* global nhm */
// import $ from 'jquery'

var Filters = nhm.Filters = function(TYPE, options){
	if(!options) options = {};
	return {
		gen: (field = 'id', op='EQ', value=0)=>{field, op, value},
	};
};

Object.assign(Filters,{
	match: (type, filters, obj)=>{
		if(filters instanceof Array) return Filters.matchAll(type, filters, obj);
		if(!(filters instanceof Object)) return false;
		const field = filters.field;
		const Type = nhm.Select.types[type];
		if(!Type || !field || !type.model[field]) return false;
		//const value = Filters.convertValueToType(filters.value, Type.model[field].type);
		const value = filters.value;
		switch((filters.op || 'EQ').toUppercase()){
			case 'EQ': return obj[field] == value;
			case 'LT': return obj[field] < value;
			case 'GT': return obj[field] > value;
			case 'LE': return obj[field] <= value;
			case 'GE': return obj[field] >= value;
			case 'NE': return obj[field] != value;
		}
		return false;
	},
	matchAll: (type, filters, obj)=>{
		return filters.filter(filter=>{
			if(filter instanceof Array)
				return Filters.matchAny(type, filters, obj);
			else
				return Filters.match(type, filter, obj).length == filters.length
		});
	},
	matchAny: (type, filters, obj)=>{
		return filters.filter(filter=>{
			if(filter instanceof Array)
				return Filters.matchAll(type, filters, obj);
			else
				return Filters.match(type, filter, obj).length > 0
		});
	},
});

export default Filters


