/* eslint-disable */
/* global nhm */
const cuid = require('../lib/cuid/index.js');
import {fetchData as get} from './data.js'
const ID = nhm.ID = cuid();

const Comms = {readys: [], watching: [], watchers: []};

Comms.ready = function(listener){
	if(listener instanceof Function) Comms.readys.push(listener)
	return new Promise(resolve=>Comms.readys.push(resolve))
}

Comms.init = function(){
	if(Comms.inited) throw Error('Comms already initialized')
	Comms.inited = true;
	(async()=>{
		const confirmation = await get('/comms/init', {terminalID: ID, watchers: Comms.watchers})
		if(confirmation.success === 201){
			Comms.readys.forEach(listener=>listener({...confirmation, ID, terminalID: ID}))
			Comms.readys.length = 0;
		} else {
			// nhm.Notify.msg('Failed to initialize Comms Module','warning')
			nhm.Log.error('Failed to initialize Comms Module', {ID, confirmation})
		}
		process.nextTick(()=>Comms.wait());
		return true;
	})().catch(nhm.Log.genFailer('Comms.init'));
};

Comms.wait = ()=>{
	get('/comms/wait', {terminalID: nhm.ID, watchers: Comms.watchers}).then(events=>{
		if(events.error == 404) return get('/comms/init', {terminalID: ID, watchers: Comms.watchers});
		if(events.error == 408) return setTimeout(()=>Comms.wait(),2000);
		if(events)console.log(events);
		if(events.error || !(events instanceof Array)) return setTimeout(()=>Comms.wait(),4000);
		events.forEach(event=>
			Comms.watching.forEach(watch=>{
				if(event.type == watch.type &&
					(!watch.method || event.method == watch.method) &&
					(!watch.id || event.id == watch.id) &&
					(!watch.tag || event.tag == watch.tag) &&
					(!watch.field || event.field == watch.field)
				) watch.watcher(event)
		}))
		setTimeout(()=>Comms.wait(),100);
	})
	.catch(err=>{
		console.log(err)
		if(err.statusCode == 500) return setTimeout(()=>get('/comms/init', {terminalID: ID, watchers: Comms.watchers}),2000);
		setTimeout(()=>Comms.wait(),10000);
	})
	.finally(()=>{
		
	})

}

Comms.watch = (matcher,watcher)=>{
	if(!(matcher instanceof Object) || matcher == null) throw TypeError('matcher needs to be an object')
	if(!(watcher instanceof Function)) throw TypeError('watcher needs to be a function')
	const watch = {watcher, matcher, ...matcher, terminalID: nhm.ID};
	try{
		// Stupid error about push not being a function when it actually is working
		get('/comms/watch', watch).then(ret=>{
			if(!ret.error){
				Comms.watching.push(watch)
				Comms.watchers.push(ret);
			} else console.log(ret)
			// console.log(Comms.watching);
		})
		.catch( nhm.Log.genFailer('Comms.wait') )
		return watch;
	}catch(e){
		console.trace(e)
	}
}

export default Comms