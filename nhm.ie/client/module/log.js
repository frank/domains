/* global nhm */

const Log = nhm.Log = {
  log: function(tag, log){
    const config = nhm.Global.config.data;
    // eslint-disable-next-line
    console.log.apply(null, log);
    return nhm.Data.fetchData(config.log, {type:'log', tag, log}).catch(e=>{});
  },
  error: function(tag, log){
    const config = nhm.Global.config.data;
    // eslint-disable-next-line
    console.error.apply(null, arguments);
    return nhm.Data.fetchData(config.log, {type:'error', tag, log}).catch(e=>{});
  },
  dir: function(tag, log){
    const config = nhm.Global.config? nhm.Global.config.data: {};
    // eslint-disable-next-line
    console.dir.apply(null, arguments);
    return nhm.Data.fetchData(config.log, {type:'dir', tag, log}).catch(e=>{});
  },
	genLogger: tag => log =>{
    const config = nhm.Global.config? nhm.Global.config.data: {};
		return nhm.Data.fetchData(config.log, {
			logType: 'gen', tag,
			log: log instanceof Object && log.message? log.message: log != null? log: 'No error data' });
	},
	genFailer: tag => err =>{
    const config = nhm.Global.config.data;
		return nhm.Data.fetchData(config.log, {
			logType: 'gen', tag,
			log: err instanceof Object && err.message? err.message: err != null? err: 'No error data' });
	}
}

export default Log
