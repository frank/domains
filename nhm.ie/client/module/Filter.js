/**
 * Filtering system for FXC frameworks
 */
var Schema = require('./Schema.js');
var moment = require('moment');

var Filters = function(proto){
	var filter = Object.assign({field: 'id', op: 'eq', value: 0},proto);
	filter.match = (record, schema)=>{
		if(!(record instanceof Object)) throw new TypeError('Parameter "record" must be an object')
		if(record[filter.field] === undefined) throw new TypeError(`Record's field "${filter.field}" not defined`)
		var fieldType = schema.getfieldType(filter.field);
		switch(filter.op){
			case 'gt': // Greater than
				return typeAdjust(record[filter.field], fieldType) > typeAdjust(filter.value, fieldType)
			case 'lt': // Less than
				return typeAdjust(record[filter.field], fieldType) < typeAdjust(filter.value, fieldType)
			case 'ge': // Greater or equal
				return typeAdjust(record[filter.field], fieldType) >= typeAdjust(filter.value, fieldType);
			case 'le': // Less or equal
				return typeAdjust(record[filter.field], fieldType) <= typeAdjust(filter.value, fieldType);
			// // 5 Features to be added later
			// case 'nu': // Null
			// 	return record[filter.field] == null;
			// case 'nn': // Not null
			// 	return record[filter.field] != null;
			// case 'li': // Like
			// 	return typeAdjust(record[filter.field], fieldType) != typeAdjust(filter.value, fieldType);
			// case 'sw': // Starts with
			// 	return typeAdjust(record[filter.field], fieldType) != typeAdjust(filter.value, fieldType);
			// case 'ew': // Ends with
			// 	return typeAdjust(record[filter.field], fieldType) != typeAdjust(filter.value, fieldType);
			case 'ne': // Not equal
				return typeAdjust(record[filter.field], fieldType) != typeAdjust(filter.value, fieldType);
			case 'eq': default: // Equal
				return typeAdjust(record[filter.field], fieldType) == typeAdjust(filter.value, fieldType);
		}
	};

	return filter;
};

function typeAdjust(value, fieldType){
	switch(fieldType.toString().toLowerCase()){
		case 'number': case 'money':
			return value == null? null:
				isNaN(value)?parseFloat(value):value;
		case 'int': case 'integer':
		case 'select': case 'selectid':
			return value == null? null:
				isNaN(value)?parseInt(value):value;
	}
	return value == null? null: ''+value;
}
Filters.typeAdjust = typeAdjust;

module.exports = Filters;
