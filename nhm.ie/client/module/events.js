/* global nhm */
import * as moment from '../lib/moment/moment.js'

var id=1;
// eslint-disable-next-line
const emits = []; //  queue of emits

const Events = nhm.Events = {
	on: (type, filters, cb)=>{
		if(!ons[type]) ons[type] = [];
		ons[type].push({id, filters, cb});
		return id++;
	},
	// eslint-disable-next-line
	emit: (type, msg)=>{},
	off: (type, id)=>{
		if(!ons[type]) ons[type] = ons[type].filter(on=>on.id!=id);
	},
}

if(nhm.Events==null) nhm.Events = {};
const ons = Events.ons = nhm.Events.ons || {test:[
	{
		id: id++,
		cb: (message)=>{
			// eslint-disable-next-line
			nhm.Log.log('Test event: "'+message+'"')
		}
	},
]}; // record of handlers by message type

const _handler = function(event){
	if(ons[event.type]) ons[event.type]
		.forEach(on=>on.cb(event.message));
};

// eslint-disable-next-line
const Data = nhm.Data('events');

var last = moment(nhm.Global.serverTime).subtract(10,'seconds');
const fetchLoop = ()=>{
	const config = nhm.Global.config;
	const matches = [];
	const filters = [matches,{field: 'createdOn', op: 'GE', value: last.format(config.timeFormat)}];
	Object.keys(ons).forEach(type=>matches.push({field: 'type',value: type}))
	const options = {
		TYPE: 'events',
		METHOD: config.wait?'wait':'list',
		data:{ filters }
	};
	nhm.Data.fetchData(config.data, options).then(events=>{
			last = moment();
			if(events instanceof Array) events.forEach(_handler);
			setTimeout(fetchLoop,10000);

	}).catch(err=>{
			// eslint-disable-next-line
			nhm.Log.error('Events fetchData error: '+JSON.stringify(err));
	});
}

Events.start = () =>{
	nhm.Comms.init()
	process.nextTick(fetchLoop);
}




export default Events
