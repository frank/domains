/* eslint-disable */
/* global nhm */
import DAO from './dao.js'
if(!window.nhm) window.nhm = {};
window.fxc = window.nhm;

/**
 * Data function
 * CRUD object builder
 * options:
 * - singleton: do not allow another instance of this CRUB object to be created. Always return first creation.
 * - singular: tag for watching record updates
 * - plural: tag for watching list updates
 * - group: TBD: tag for watching selection updates (Not implemented yet.)
 * @param {string} TYPE The tag name of the schema to use on the server side.
 * @param {string} options Additional configuration parameters
 * @returns {object} CRUD object with crud functions to access the database
 */
const Data = nhm.Data = function(TYPE, options){
	// // Singleton parameter ensures CRUD object reused. Fixing options.
	// if(Data.types && Data.types[TYPE] && Data.types[TYPE].options && Data.types[TYPE].options.singleton){
	// 	return Data.types[TYPE];
	// }
	// If options given or 'savedType' set in options then new DAO object created
  if(!options){
		if(!(Data.types instanceof Object) || Data.types==null){
			Data.types = {};
			options = {savedType: true};
		}
		if(Data.types[TYPE]) return Data.types[TYPE];
	};
	if(!(options instanceof Object) || options==null) options = {};
  Object.assign(options, {_csrf: nhm._csrf});
	// if(Data.types[TYPE]) return Data.types[TYPE];
  return Data.types[TYPE] = {
		options,
    create: (record)=>{
      return new Promise((resolve, reject)=>{
        const config = nhm.Global.config.data;
        if(!(Data.creates instanceof Array)) Data.creates = [];
        var create = {record, resolve, reject};
        Data.creates.push(create);
        if(Data.creates.length == 1) doNext();

        async function doCreate(record){
          return await fetchData(config.uri, Object.assign({}, options, {record},
            {TYPE, METHOD: 'create'}))
        }

        function doNext(){
          const create = Data.creates[0];
          if(create) doCreate(create.record).then(res=>{
            Data.creates.shift();
            create.resolve(res);
            process.nextTick(doNext);
          }).catch(err=>{
            Data.creates.shift();
            create.reject(err);
            process.nextTick(doNext);
          });
        }
      })
    },

    update: (record)=>{
      return new Promise((resolve, reject)=>{
        const config = nhm.Global.config.data;
        if(!(Data.updates instanceof Array)) Data.updates = [];
        var update = {record, resolve, reject};
        Data.updates.push(update);
        if(Data.updates.length == 1) doNext();

        async function doUpdate(record){
          return await fetchData(config.uri, Object.assign({}, options, record,
            {TYPE, METHOD: 'update'}))
        }

        function doNext(){
          const update = Data.updates[0];
          if(update) doUpdate(update.record).then(res=>{
            Data.updates.shift();
            update.resolve(res);
            process.nextTick(doNext);
          }).catch(err=>{
            Data.updates.shift();
            update.reject(err);
            process.nextTick(doNext);
          });
        }
      })
    },

    change: (change)=>{
      return new Promise((resolve, reject)=>{
        const config = nhm.Global.config.data;
        if(!(Data.querys instanceof Array)) Data.querys = [];
        var query = {change, resolve, reject};
        Data.querys.push(query);
        if(Data.querys.length == 1) doNext();

        async function doChange(change){
          return await fetchData(config.uri, Object.assign({}, options, change,
            {TYPE, METHOD: 'change'}))
        }

        function doNext(){
          const query = Data.querys[0];
          if(query) doChange(query.change).then(res=>{
            Data.querys.shift();
            query.resolve(res);
            process.nextTick(doNext);
          }).catch(err=>{
            Data.querys.shift();
            query.reject(err);
            process.nextTick(doNext);
          });
        }
      })
    },

    delete: async (id)=>{
      const config = nhm.Global.config.data;
      return await fetchData(config.uri, Object.assign({}, options,
        {TYPE, METHOD: 'delete', id}));
    },
    read: async (data)=>{
      const config = nhm.Global.config.data;
      return await fetchData(config.uri, Object.assign({}, options, data,
        {TYPE, METHOD: 'read', data}));
    },
    list: async (data)=>{
      const config = nhm.Global.config.data;
      return await fetchData(config.uri, Object.assign({}, options, data,
        {TYPE, METHOD: 'list'}));
    },
    count: async (data)=>{
      const config = nhm.Global.config.data;
      return await fetchData(config.uri, Object.assign({}, options,
        {TYPE, METHOD: 'count', data}));
    },
  };
};

export const fetchData = Data.fetchData = async (url = ``, data = {}, method = null)=>{
	const config = nhm.Global.config.data;
  // Default options are marked with *
	const res = await fetch(url, {
			method: method || config.method || 'post', // *GET, POST, PUT, DELETE, etc.
			mode: "cors", // no-cors, cors, *same-origin
			cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
			// credentials: "same-origin", // include, *same-origin, omit
			headers: {
					"Content-Type": "application/json; charset=utf-8",
					// "Content-Type": "application/x-www-form-urlencoded",
			},
			// redirect: "follow", // manual, *follow, error
			// referrer: "no-referrer", // no-referrer, *client
			body: JSON.stringify(data), // body data type must match "Content-Type" header
	})
	const contentType = res.headers.get('content-type');
	try{
		return await res.json();
	} catch(e){
		// throw new Error('Non JSON data returned');
		return {error: 500, message: e.message? e.message: e};
	}
}

export const post = Data.post = Data.fetchData;
export const get = Data.get = (url, data, method = 'GET') => Data.fetchData(url, data, method)

export default Data


