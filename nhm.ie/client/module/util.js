import RecordArray from './RecordArray.js'
import * as cuid from '../lib/cuid/index.js';
import Base64 from './base64.js'
import * as sha256 from '../lib/js-sha256/build/sha256.min.js'

const Util = {}
export default Util
Util.RecordArray = RecordArray


Util.Watch = function(types){
	const Watch = {

		// Add tag type
		addType(type){
			return Watch.watchers[type] = Watch.watchers[type] || [];
		},

		// Verify tag type
		isType(type){
			return Watch.watchers[type] instanceof Array;
		},

		// Collections of watches by tag
		watchers: {},

		/**
		 * Set up a watcher for a specific tag
		 * @param {string} tag 
		 * @param {function} watcher
		 * @returns {string} cuid ID of watch record for use with unwatch
		 * @throws {Error}
		 */
		watch(tag, watcher){
			if(!(tag instanceof String) || tag == null)
				throw TypeError('tag string required')
			if(Object.keys(Watch.watchers).indexOf(tag)==-1)
				throw Error('Invalid tag');
			if(!(watcher instanceof Object) || watcher == null)
				throw TypeError('Watcher needs to be a function')
			const watch = {id: cuid(), run: watcher};
			Watch.watchers[tag].push(watch);
			return watch.id;
		},

		/**
		 * Removes watch record
		 * @param {string} id cuid of watch record to unwatch
		 */
		unwatch(id){
			if(!id) return;
			Object.keys(Watch.watchers).forEach(tag=>
				Util.removeRecords(Watch.watchers[tag], 'id', id)
			);
		},

		/**
		 * Alerts watchers of a tag with a value or values
		 * @param {string} tag
		 * @param {any} value
		 */
		emit(tag){
			if(!(tag instanceof String) || tag == null)
				throw TypeError('tag string required')
			if(Object.keys(Watch.watchers).indexOf(tag)==-1)
				throw Error('Invalid tag');
			const args = Array.prototype.split.call(arguments);
			args.shift();
			if(args.length == 0) args.push(true);
			Watch.watchers[tag].forEach(f=>f.run.apply(null, args))
		}
	}
	if(types instanceof Array)
		types.forEach(type=>Watch.addType(type));
	return Watch;
}

Util.merge = function(arr){
	if(!(arr instanceof Array)) return arr;
	for(var i=1;i<arguments.length;i++){
		if(arguments[i] instanceof Array)
			for(var j=0;j<arguments[i].length;j++)
				arr.push(arguments[i][j]);
		else
			arr.push(arguments[i]);
	}
	return arr;
};

Util.bind = function bind(func, target){
	return function(){
		func.apply(target, arguments);
	};
};

Util.randomString = function (len, charSet) {
	charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	len = len || 8;
	var randomString = '';
	for (var i = 0; i < len; i++) {
		var randomPoz = Math.floor(Math.random() * charSet.length);
		randomString += charSet.substring(randomPoz,randomPoz+1);
	}
	return randomString;
};

Util.decodeHtmlEntity = function(str) {
	return str.replace(/&#(\d+);/g, function(match, dec) {
		return String.fromCharCode(dec);
	});
};

Util.encodeHtmlEntity = function(str) {
	var buf = [];
	for (var i=str.length-1;i>=0;i--) {
		buf.unshift(['&#', str[i].charCodeAt(), ';'].join(''));
	}
	return buf.join('');
};

Util.genFilter = function(f,o,v,g,i){return {field:f,op:o,value:v,group:g||null,id:i||null};};

// Generate an ordering function depending on ordering required for a field and take a second set of field and ordering for second comparison.
Util.compareFn = function(field, order, field2, order2){
	order = order == 'DESC'? 'DESC': 'ASC';
	return {
		DESC: function(a,b){
			var c=((a[field] instanceof String)?a[field].trim():a[field])+'';
			var d=((b[field] instanceof String)?b[field].trim():b[field])+'';
			if(isNaN(c) && isNaN(d))
				return c > d? -1: c < d? 1: 
					field2? Util.compareFn(field2, order2)(a,b): 0;
			else
				return d - c? d - c:
					field2? Util.compareFn(field2, order2)(a,b): 0;

		},
		ASC: function(a,b){ 
			var c=((a[field] instanceof String)?a[field].trim():a[field])+'';
			var d=((b[field] instanceof String)?b[field].trim():b[field])+'';
			if(isNaN(c) && isNaN(d))
				return c < d? -1: c > d? 1: 
					field2? Util.compareFn(field2, order2)(a,b): 0;
			else
				return c - d? c - d: 
					field2? Util.compareFn(field2, order2)(a,b): 0;
		},
	}[order];
};

// Sort record by 2 sets of field and order.
Util.sortRecords = Util.sortBones = function(arr, field, order, field2, order2, trim){
	if(!(arr instanceof Array)) return false;
	if(!field) return false;
	if(order != 'DESC') order = 'ASC';
	arr.sort(Util.compareFn(field, order, field2, order2, trim));
	return arr;
};

Util.findRecord = Util.findBone = function(arr, key, value){
	if(!(arr instanceof Array)) return false;
	if(value === null) return false;
	if(value === undefined) return false;
	for(var i = 0; i < arr.length; i++)
		if(arr[i][key] && arr[i][key].toString() === value.toString()) return arr[i];
	return false;
};

Util.findRecords = Util.findBones = function(arr, key, values, anyType){
	var bones = [];
	if(!(arr instanceof Array)) return bones;
	//if(!(key instanceof String)) return bones;
	if(!(values instanceof Array)) values = [values];
	for(var i = 0; i < arr.length; i++)
		if(anyType && values.indexOf(''+arr[i][key]) != -1) bones.push(arr[i]);
		else if(anyType && values.indexOf(0+arr[i][key]) != -1) bones.push(arr[i]);
		else if(values.indexOf(arr[i][key]) != -1) bones.push(arr[i]);
	return bones;
};

Util.matchRecords = Util.matchBones = function(arr, key, values){
	var bones = [];
	if(!(arr instanceof Array)) return bones;
	//if(!(key instanceof String)) return bones;
	if(values instanceof String || !(values instanceof Array)) values = [values];
	values = [].concat(values);
	for(var i = 0; i < values.length; i++)
		bones[i] = Util.findRecord(arr, key, values[i]);
	//if(values.indexOf(arr[i][key]) != -1) bones.push(arr[i]);
	return bones;
};

Util.findRecordsWithFieldSet = function(arr, field){
	var bones = [];
	if(!(arr instanceof Array)) return bones;
	//if(!(key instanceof String)) return bones;
	if(field !== undefined && field !== null)
		for(var i = 0; i < arr.length; i++)
			if(arr[i][field] !== undefined && arr[i][field] !== null) bones.push(arr[i]);
	return bones;
};

Util.cloneRecords = Util.cloneBones = function(arr){
	if(!(arr instanceof Array)) return [];
	var clone = [];
	for(var i = 0; i < arr.length; i++)
		clone.push( Object.assign({}, arr[i]) );
	return clone;
};

Util.clearField = function(arr, field){
	if(!field) return arr;
	if(!(arr instanceof Array)) return [];
	for(var i = 0; i < arr.length; i++)
		if(arr[i][field] !== undefined) delete arr[i][field];
	return arr;
};

Util.setField = function(arr, field, value, all){
	if(!field) return arr;
	if(!(arr instanceof Array)) return [];
	for(var i = 0; i < arr.length; i++)
		if(arr[i][field] !== undefined || all) arr[i][field] = value;
	return arr;
};

Util.listValues = function(arr, key){
	var list = [];
	for(var i = 0; i < arr.length; i++)
		list.push(arr[i][key]);
	return list;
};

Util.findValues = function(arr, key){
	var list = [];
	for(var i = 0; i < arr.length; i++)
		if(arr[i][key] !== undefined && arr[i][key] !== null)
			list.push(arr[i][key]);
	return list;
};

Util.uniqueValues = function(arr, key){
	var list = [];
	for(var i = 0; i < arr.length; i++)
		if(list.indexOf(arr[i][key])==-1 && arr[i][key] !== undefined && arr[i][key] !== null)
			list.push(arr[i][key]);
	return list;
};

Util.removeRecords = Util.removeBones = function(arr, key, values, anyType){
	if(!(arr instanceof Array)) return 0;
	var found = 0;
	if(!(values instanceof Array)) values = [values];
	for(var i = 0; i < arr.length; i++)
		if((anyType && values.indexOf(''+arr[i][key]) != -1) ||
			(anyType && values.indexOf(0+arr[i][key]) != -1) ||
			(values.indexOf(arr[i][key]) != -1)){
			for(var j = i--; j < arr.length; j++)
				arr[j] = arr[j + 1];
			arr.length--;
			found++;
		}
	return found;
};

Util.copyFields= function(target, source, fields, id){
	var i;
	if(!id) id = 'id';
	if(!(source instanceof Array)) return target;
	if(!(fields instanceof Array)) fields = Object.keys(source);
	if(target instanceof Array && source instanceof Array){
		for(i = 0; i < source.length; i++){
			var sRecord = Util.findBone(source, id, source[i].id);
			var tRecord = Util.findBone(target, id, source[i].id);
			if(!tRecord) target.push(tRecord = {id: source[i].id});
			for(var j = 0; j < fields.length; j++)
				tRecord[fields[j]] = sRecord[fields[j]];
		}
	}
	else
		for(i = 0; i < fields.length; i++)
			target[fields[i]] = source[fields[i]];
	return target;
};

Util.copyFieldsCheckedOnly= function(target, source, fields, id){
	var i;
	if(!id) id = 'id';
	if(!(source instanceof Array)) return target;
	if(!(fields instanceof Array)) fields = Object.keys(source);
	if(target instanceof Array && source instanceof Array){
		for(i = 0; i < source.length; i++){
			var sRecord = Util.findBone(source, id, source[i].id);
			var tRecord = Util.findBone(target, id, source[i].id);
			if(!tRecord && sRecord.check) target.push(tRecord = {id: source[i].id});
			for(var j = 0; j < fields.length; j++)
				tRecord[fields[j]] = sRecord[fields[j]];
		}
	}
	else
		for(i = 0; i < fields.length; i++)
			target[fields[i]] = source[fields[i]];
	return target;
};

Util.removeDuplicates = function(arr){
	if(!(arr instanceof Array)) return arr;
	var second;
	for(var i=0;i<arr.length;i++)
		while((second = arr.indexOf(arr[i],arr.indexOf(arr[i]) + 1)) > 0)
			arr.splice(second,1);
};

Util.genRecordSet = function(arr, field){
	var recs = [];
	for(var i=0;i<arr.length;i++){
		var rec = {id: 1+i};
		rec[field || 'name'] = arr[i];
		recs.push(rec);
	}
	return recs;
};

Util.fixContext = function(func, context) {
	return function() {
		return func.apply(context, arguments);
	};
};

Util.fixContexts = function(object) {
	for (var func in object)
		if (typeof (object[func]) === 'function')
			object[func] = Util.fixContext(object[func], object);
	return object;
};

Util.lookup = function(obj, field) {
	if (!obj) { return null; }
	var chain = field.split(']').join('').split('[');
	for (var i = 0, len = chain.length; i < len; i++) {
		var prop = obj[chain[i]];
		if (typeof(prop) === 'undefined') { return null; }
		if (typeof(prop) !== 'object') { return prop; }
		obj = prop;
	}
	return null;
};

Util.numberWithCommas = function(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

Util.model2columns = function(model){
	if(!model) return [];
	var keys = Object.keys(model);
	var columns = [];
	if(!(keys instanceof Array) || !keys || !model[keys[0]] || !model[keys[0]].field) return [];
	for(var i=0;i<keys.length;i++)
		columns.push(Object.assign( {id: keys[i]} , model[ keys[i] ] ));
	return columns;
};

Util.jsonToArray = function(jsonArray){
	var s = [];
	try {
		s = JSON.parse(jsonArray);
	} catch(e) {
		s = [];
	}
	return s;
};

Util.arrayToJson = function(array, clearHashKey){
	var s = '[]';
	if(!(array instanceof Array)) return s;
	if(clearHashKey)
		Util.clearField(array, '$$hashKey');
	try {
		s = JSON.stringify(array);
	} catch(e) {
		s = '[]';
	}
	return s;
};

Util.jsonToObject = function(jsonObject){
	var s = {};
	try {
		s = JSON.parse(jsonObject);
	} catch(e) {
		s = {};
	}
	return s;
};

Util.objectToJson = function(object, pretty){
	var s = '{}';
	if(!(object instanceof Object)) return s;
	try {
		s = JSON.stringify(object, pretty?null:undefined, pretty?2:undefined);
	} catch(e) {
		s = '{}';
	}
	return s;
};

Util.jsonToAny = function(jsonObject){
	var s = null;
	try {
		s = JSON.parse(jsonObject);
	} catch(e) {
		s = null;
	}
	return s;
};

Util.anyToJson = function(object){
	var s = 'null';
	try {
		s = JSON.stringify(object);
	} catch(e) {
		s = 'null';
	}
	return s;
};

Util.hex2str = hex=>{
    var str = '';
    for (var i = 0; i < hex.length; i += 2) {
        var v = parseInt(hex.substr(i, 2), 16);
        str += Util.chra(v % 256);// += chr(v % 256);
    }
    return str;
}

Util.hash = str=>{ // btoa not encode as encoding bytes not 2byte UTF16 Characters.
	return Base64.btoa(Util.hex2str(sha256(str)));
};

Util.chra = ch=>(
	'\x00'+'\x01'+'\x02'+'\x03'+'\x04'+'\x05'+'\x06'+'\x07'+'\x08'+'\x09'+'\x0A'+'\x0B'+'\x0C'+'\x0D'+'\x0E'+'\x0F'+
	'\x10'+'\x11'+'\x12'+'\x13'+'\x14'+'\x15'+'\x16'+'\x17'+'\x18'+'\x19'+'\x1A'+'\x1B'+'\x1C'+'\x1D'+'\x1E'+'\x1F'+
	'\x20'+'\x21'+'\x22'+'\x23'+'\x24'+'\x25'+'\x26'+'\x27'+'\x28'+'\x29'+'\x2A'+'\x2B'+'\x2C'+'\x2D'+'\x2E'+'\x2F'+
	'\x30'+'\x31'+'\x32'+'\x33'+'\x34'+'\x35'+'\x36'+'\x37'+'\x38'+'\x39'+'\x3A'+'\x3B'+'\x3C'+'\x3D'+'\x3E'+'\x3F'+
	'\x40'+'\x41'+'\x42'+'\x43'+'\x44'+'\x45'+'\x46'+'\x47'+'\x48'+'\x49'+'\x4A'+'\x4B'+'\x4C'+'\x4D'+'\x4E'+'\x4F'+
	'\x50'+'\x51'+'\x52'+'\x53'+'\x54'+'\x55'+'\x56'+'\x57'+'\x58'+'\x59'+'\x5A'+'\x5B'+'\x5C'+'\x5D'+'\x5E'+'\x5F'+
	'\x60'+'\x61'+'\x62'+'\x63'+'\x64'+'\x65'+'\x66'+'\x67'+'\x68'+'\x69'+'\x6A'+'\x6B'+'\x6C'+'\x6D'+'\x6E'+'\x6F'+
	'\x70'+'\x71'+'\x72'+'\x73'+'\x74'+'\x75'+'\x76'+'\x77'+'\x78'+'\x79'+'\x7A'+'\x7B'+'\x7C'+'\x7D'+'\x7E'+'\x7F'+
	'\x80'+'\x81'+'\x82'+'\x83'+'\x84'+'\x85'+'\x86'+'\x87'+'\x88'+'\x89'+'\x8A'+'\x8B'+'\x8C'+'\x8D'+'\x8E'+'\x8F'+
	'\x90'+'\x91'+'\x92'+'\x93'+'\x94'+'\x95'+'\x96'+'\x97'+'\x98'+'\x99'+'\x9A'+'\x9B'+'\x9C'+'\x9D'+'\x9E'+'\x9F'+
	'\xA0'+'\xA1'+'\xA2'+'\xA3'+'\xA4'+'\xA5'+'\xA6'+'\xA7'+'\xA8'+'\xA9'+'\xAA'+'\xAB'+'\xAC'+'\xAD'+'\xAE'+'\xAF'+
	'\xB0'+'\xB1'+'\xB2'+'\xB3'+'\xB4'+'\xB5'+'\xB6'+'\xB7'+'\xB8'+'\xB9'+'\xBA'+'\xBB'+'\xBC'+'\xBD'+'\xBE'+'\xBF'+
	'\xC0'+'\xC1'+'\xC2'+'\xC3'+'\xC4'+'\xC5'+'\xC6'+'\xC7'+'\xC8'+'\xC9'+'\xCA'+'\xCB'+'\xCC'+'\xCD'+'\xCE'+'\xCF'+
	'\xD0'+'\xD1'+'\xD2'+'\xD3'+'\xD4'+'\xD5'+'\xD6'+'\xD7'+'\xD8'+'\xD9'+'\xDA'+'\xDB'+'\xDC'+'\xDD'+'\xDE'+'\xDF'+
	'\xE0'+'\xE1'+'\xE2'+'\xE3'+'\xE4'+'\xE5'+'\xE6'+'\xE7'+'\xE8'+'\xE9'+'\xEA'+'\xEB'+'\xEC'+'\xED'+'\xEE'+'\xEF'+
	'\xF0'+'\xF1'+'\xF2'+'\xF3'+'\xF4'+'\xF5'+'\xF6'+'\xF7'+'\xF8'+'\xF9'+'\xFA'+'\xFB'+'\xFC'+'\xFD'+'\xFE'+'\xFF'
)[ch];