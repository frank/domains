/* eslint-disable */
/* global nhm */
if(!window.nhm) window.nhm = {};
window.fxc = window.nhm;
if(!window.nhm.Global) window.nhm.Global = {};

window.require=function(e,n,v1){function t(e,o,u,a){if(e.g)return o(e.e,e);var c=e.g=e.l,f=new XMLHttpRequest;f.onload=function(i,l){function s(){l--||o(n,e)}200==f.status||e.t?(i=[],(e.t=e.t||f.response).replace(/(?:^|[^\w\$_.])require\s*\(\s*["']([^"']*)["']\s*\)/g,function(e,n){i.push(n)}),l=i.length,i.map(function(o){t(r(e.l,o),s,"."!=o[0]?c+"/../":n,o)}),s()):u?t(e.n=r(u+="../",a),o,u,a):(e.e=f,o(f,e))},e.t?f.onload():(f.open("GET",c,!0),f.send())}function r(e,n,t){if(e.e)throw e.e;return n?(f.href=e,i.href="."!=n[0]?"./node_modules/"+n:n,t=i.href+".js",f.href="",u[t]=u[t]||{l:t}):e.n?r(e.n):(e[c]||(e.f||a("(function(require,"+c+",module){"+e.t+"\n})//# sourceURL="+e.l))(function(n){return r(r(e.l,n))},e[c]={},e),e[c])}function o(e,n){t(e.call?{l:"",t:""+e,f:e}:r("",e),function(t,o){try{e=r(o)}catch(u){t=u}n&&n(t,e)})}var u={},a=eval,c="createElement",f=e[c]("base"),i=e[c]("a");return e.head.appendChild(f),c=e.querySelector("script[data-main]"),c&&o(c.dataset.main),c="exports",o}(document);

const Global = nhm.Global;
Global.config = {
	data: {
    tag: 'default',
    data:{
      name: 'Default Config',
      tag: 'default',
      uri:'/data',
      type: 'json',
      method: 'POST',
      log:'/log',
      auth:'/user',
    }
  }
};

const Config = Global.Config = {}
Config.configs = Global.configs = Global.configs==null? []: Global.configs;
Config.add = (config)=>{
	if(!config || !config.data) return false;
	let configs = Config.configs.filter(conf=>{return conf.data.tag == config.data.tag});
	if(configs.length<1)
		Config.configs.push(config)
	return true
}
Config.use = (tag)=>{
	let config = Config.configs.filter(config=>{return config.data.tag == tag});
	Global.config = config[0] || Global.config || Global.configs[0];
}

Config.remove = (tag)=>{
	if(tag == 'default')
		throw new Error('Cannot remove default config');
	if(Global.config.tag == tag)
		throw new Error('Cannot remove current config');
	return Config.configs.remove('tag',tag);
}

export default Global;
