/* eslint-disable */
/* global nhm */
export const defaultUser = {type: 'guest', name: 'guest', id: 0};

// if(!window.nhm) window.nhm = {};
// if(!nhm.Global) nhm.Global = {};
// if(!nhm.Global.config) nhm.Global.config = {};
// const config = nhm.Global.config;

console.log('user:', nhm.Global.user)
const User = nhm.User = {user: nhm.Global.user || defaultUser};
// User.events = new EventEmitter();

User.watchers = {user:[]};

User.nextWatcherID = 1;
User.watch=(tag, watcher)=>{
	if(Object.keys(User.watchers).indexOf(tag)==-1) throw Error('Invalid tag');
	User.watchers[tag].push({id: User.nextWatcherID, run: watcher});
	return User.nextWatcherID++;
}
User.unwatch=(id)=>{
	Object.keys(User.watchers).forEach(tag=>
		nhm.Util.removeRecords(User.watchers[tag], 'id', id)
	);
}

User.getUser = async credentials => {
	try{
		User.lastError = null;
		if(!nhm.Global.config || !nhm.Global.config.data || !nhm.Global.config.data.auth){
			console.log(nhm.Global)
			throw new Error('Config not initialized for auth');
		}
		const url = nhm.Global.config.data.auth+'/data.json';
		// const res = await fetch(url, {
		// 	method: nhm.Global.config.method || 'POST',
		// 	mode: 'cors',
		// 	cache: "no-cache",
		// 	headers:{ "Content-Type": "application/json; charset=utf-8" },
		// })
		// const {user} = nhm.Global.session = await res.json();

		let {user} = nhm.Global.session = await nhm.Data.fetchData(url,{},'post')

		if(!user.id) user = defaultUser;
		User.user = user;
		User.watchers.user.forEach(f=>f.run(user))
		return user;
	}catch(err){
		User.lastError = err;
		console.error(err);
		return defaultUser;
	}
};

setTimeout(()=>{
	User.getUser().catch(nhm.Log.genLogger('User.init.getUser'));
},1);

User.login = async credentials => {
	User.lastError = null;
	try{
		const res = await nhm.Data.fetchData(
			nhm.Global.config.data.auth+'/login',
			{...credentials, 'returnJson': true}
		)
		if(res.error){
			alert(res.message);
			return User.logout().then(nhm.Global.navTo('/user/login'));
		}
		return await User.getUser().then(nhm.Global.navTo('/user/welcome'));
	}catch(err){
		User.lastError = err;
		console.error(err);
		return defaultUser;
	}
};

User.logout = async () => {
	try{
		const res = await fetch(nhm.Global.config.data.auth+'/logout', {
			method: 'POST',
			mode: 'cors',
			cache: "no-cache",
			headers:{ "Content-Type": "application/json; charset=utf-8" },
			body: JSON.stringify({return: true})
		})
		User.lastError = null;
		return await User.getUser();
	}catch(err){
		User.lastError = err;
		console.error(err)
		return defaultUser;
	}
};

export default User;
// export const fetchData = Data.fetchData = (url = ``, data = {})=>{
//   // Default options are marked with *
//     return fetch(url, {
//         method: 'post' || config.method, // *GET, POST, PUT, DELETE, etc.
//         mode: "cors", // no-cors, cors, *same-origin
//         cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
//         // credentials: "same-origin", // include, *same-origin, omit
//         headers: {
//             "Content-Type": "application/json; charset=utf-8",
//             // "Content-Type": "application/x-www-form-urlencoded",
//         },
//         // redirect: "follow", // manual, *follow, error
//         // referrer: "no-referrer", // no-referrer, *client
//         body: JSON.stringify(data), // body data type must match "Content-Type" header
//     })
//     .then(res => {
//       const contentType = res.headers.get('content-type');
//       if(contentType && contentType.indexOf('application/json') !== -1)
//         return res.json();
//       else
//         throw new Error('Non JSON data returned');
//     }); // parses response to JSON
// }
