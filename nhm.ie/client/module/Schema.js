var list = [];
var Schema = module.exports = function(type){
	return {
		convertValueToField: function(value, field){
			if(value==null) return null;
			switch(this.getType(field)){
				case 'number': return isNaN(value)? parseFloat(value): value;
				case 'string': default:
					return value.toString();
			}
		}
	};
};

// This really needs to be implemented to be useful.
Schema.isType = type=>true;
Schema.cleanTypeName = type=>typeof(type)=='string'?type.replace(/[^a-zA-Z]/,''):'';

// TBD
// {
// 	loadFolder: function(path){};
// 	add: function(name, schema){};
// 	remove: function(name){};
// 	replace: function(name, schema){};
// 	list: function(){};
// };