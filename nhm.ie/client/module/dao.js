/* eslint-disable */
/* global nhm */
const cuid = require('../lib/cuid/index.js');

const DAO = function(TYPE, options){
	if(!(DAO.types instanceof Object) || DAO.types==null) DAO.types = {};
  if(!options){
		if(DAO.types[TYPE]) return DAO.types[TYPE];
		options = {savedType: true};
	};
	const singular = options.singular || 'record'; // Override the name of the singular record paramater
	const plural = options.plural || 'records'; // Override the name of the plural list paramater
	const dao = DAO.types[TYPE] = {
		reload(options){
			if(!(options instanceof Object) || options==null) options = {};
			if(options.filters){
				dao.filters = options.filters;
			} else if(dao.filters){
				options.filters = dao.filters;
			} else options.filters = [];
			const schema = nhm.Select.types[TYPE];
			if(
				schema && schema.meta && schema.meta.restrict &&
				schema.meta.restrict.list == 'owner' &&
				schema.meta.owner && nhm.Global.user.id && nhm.Global.user.type != 'admin'
			){
				options.filters.push({field: schema.meta.owner, value: nhm.Global.user.id})
				schema.meta.owner
			}
			nhm.Data(TYPE)
			.list(options)
			.then(records=>{
				dao.records = records;
				dao.watchers[plural].forEach(f=>f.run(records))
			})
			.catch(nhm.Log.genLogger(`LHIM.dao.${TYPE}.${plural}.load`));
			return 0;
		},
		load(id){
			// if(dao.loading){
			// 	if(dao.loading == 2)
			// 		dao.loadPromise = dao.loadPromise.then(()=>dao.load(id))
			// 	dao.loading = 2;
			// 	return dao.loadPromise;
			// }
			dao.loading = 1;
			if(id==null){
				if(dao.record)
					id = dao.record.id;
				else
					return console.log('ID required to load')
			}
			dao.loadPromise = new Promise((resolve, reject)=>{
				nhm.Data(TYPE).read({id:id})
				.then(record=>{
					dao.record = record;
					dao.watchers[singular].forEach(f=>f.run(record))
					process.nextTick($=>resolve(record))
				}).catch(err=>{
					nhm.Log.genLogger(`LHIM.dao.${TYPE}.${singular}.load`)
					console.error(err)
					process.nextTick($=>resolve(err))
				}).finally(()=>{
					if(dao.loading == 2) process.nextTick(()=>dao.load(id))
					dao.loading = 0;
				});
			});
			return dao.loadPromise;
		},
		records: null,
		record: null,
		watchers: {[plural]:[],[singular]:[]},
		watch(tag, watcher){
			if(Object.keys(dao.watchers).indexOf(tag)==-1) throw Error('Invalid tag');
			const watch = {id: cuid(), run: watcher};
			dao.watchers[tag].push(watch);
			return watch.id;
		},
		unwatch(id){
			Object.keys(dao.watchers).forEach(tag=>
				nhm.Util.removeRecords(dao.watchers[tag], 'id', id)
			);
		},
		changed(field){
			return nhm.Data(TYPE).change({
				id: dao.record.id,
				field: field,
				value: dao.record[field]
			})
		}
	}

	if(options.watching) nhm.Comms.watch({type: TYPE},event=>{
		switch(event.method){
			case 'update': case 'change':	dao.load(); break;
			case 'create': case 'delete': dao.reload();
		}
	})

	return dao;
}

export default DAO;