/* RecordArray
 * Optional replacement for Array to operate as a array of records.
 * 
 * @author Francis Carelse
 * @version 0.4.3.23
 */

/* eslint-disable */
/* RecordArray
 * Optional replacement for Array to operate as a array of records.
 * 
 * @author Francis Carelse
 * @version 0.4.4.23
 */
import Records from './Records.js';

/**
 * @description: An extension of Array that provides record processing related methods
 * @author 'Francis Carelse'
 */
export default class RecordArray extends Array{
	/**
	 * @constructor
	 * @param {Array} array 
	 */
	constructor(array){
		super();
		var _this = this;
		// If supplied array parameter is an Array
		if(array instanceof Array)
			// Then iterate over each object in it
			array.forEach(function(record){
			// Push a duplicate of record
	 		_this.push(Object.assign({},record));
	 	});
	}

	findOne(key, value, strict){
		var arr = this;
		strict = !!strict;
		if(value === null) return false;
		if(value === undefined) return false;
		for(var i = 0; i < arr.length; i++)
			if(arr[i][key] && ((!strict && arr[i][key] == value) || arr[i][key] === value)) return arr[i];
		return false;
	};

	findOneById(value, strict){
		return this.findOne('id', value, strict)
	}
	
	findOneByTag(value, strict){
		return this.findOne('tag', value, strict)
	}

	sortBy(field, order){
		// Assert field parameter is a string.
		if(typeof(field)!='string')
			throw new TypeError('String expected for first parameter.');
		// Assert order parameter is ASC or DESC
		if(typeof(order)!='string' || !(order.toLowerCase() == 'asc' || order.toLowerCase() == 'desc'))
			throw new TypeError('\'ASC\' or \'DESC\' String expected for second parameter.');
		//Return sorted using appropriate function
		return this.sort((order.toLowerCase() == 'asc')?
			sortFnASC:
			sortFnDESC);
		// Sorting Ascending Strategy
		function sortFnASC(a,b){
			var c =
				// Evaluate to 0 f equal
				(a[field]==b[field]?0:
				// 1 indicates wrong order. -1 indicates correct order
				(a[field]>b[field]?1:-1));
			// This does not work if you do not assign to a variable before returning.
			return c;
		}
		// Sorting Descending Strategy (just reverse the testing parameters)
		function sortFnDESC(a,b){ return sortFnASC(b,a) }
	}
	
	/**
	 * Sort this RecordArray by a set of fields in ascending order
	 * Takes an array of strings or a space separated string of fieldnames
	 * @param {Array<String> | String} fields
	 */
	sortASC(fields){
		// If fields parameter is not already an Array
		if(!(fields instanceof Array))
			// Ensure is string and split space separated fieldnames
			fields = fields.toString().split(' ');
		// Throw out any non string fields
		fields = fields.filter(f=>typeof f == 'string')
		// If no fields left then abort
		if(!fields.length) throw new TypeError('Parameter "fields" needs to be an array of strings or space separated list of field names');
		// Return sort using item pair evaluation strategy
		return this.sort(function(a,b){
			// Iterate over fields list
			for(var i=0;i<fields.length;i++)
				// Sequentially check for the first instance of inequality
				if(a[fields[i]]!=b[fields[i]])
					// If wrong order then pass back 1 otherwise -1
					return a[fields[i]]>b[fields[i]]?1:-1;
			// All fields are equal so return 0 for matching
			return 0;
		});
	}

	/**
	 * Sort this RecordArray by a set of fields in descending order
	 * Takes an array of strings or a space separated string of fieldnames
	 * @param {Array<String> | String} fields
	 */
	sortDESC(fields){
		// If fields parameter is not already an Array
		if(!(fields instanceof Array))
			// Ensure is string and split space separated fieldnames
			fields = fields.toString().split(' ');
		// Throw out any non string fields
		fields = fields.filter(f=>typeof f == 'string')
		// If no fields left then abort
		if(!fields.length) throw new TypeError('Parameter "fields" needs to be an array of strings or space separated list of field names');
		// Return sort using item pair evaluation strategy
		return this.sort(function(a,b){
			// Iterate over fields list
			for(var i=0;i<fields.length;i++)
				// Sequentially check for the first instance of inequality
				if(a[fields[i]]!=b[fields[i]])
					// If wrong order then pass back 1 otherwise -1
					return a[fields[i]]<b[fields[i]]?1:-1;
			// All fields are equal so return 0 for matching;
			return 0;
		});
	}

	/**
	 * Clone this RecordArray or supplied Array of reords to a new RecordArray
	 * @param {Array} arr 
	 */
	clone(arr){
		// If no source array supplied then use this one
		arr = arr || this;
		// Create new RecordArray
		var clone = new RecordArray();
		for(var i = 0; i < arr.length; i++)
			clone.push( Object.assign({}, arr[i]) );
		return clone;
	};

	/**
	 * @returns 'Array of cloned records'
	 */
	toArray(){
		// Clone to an Array
		return this.map(record=>Object.assign({},record));
	}

	// TBD
	read(options, filters){
		throw Error('Function yet to be developed')
	}
	list(options, filters){
		throw Error('Function yet to be developed')
	}
	create(options, filters){
		throw Error('Function yet to be developed')
	}
	update(options, filters){
		throw Error('Function yet to be developed')
	}
	delete(options, filters){
		throw Error('Function yet to be developed')
	}
	
}

RecordArray.prototype.find = function(key, values, anyType){
	var arr = this;
	var bones = [];
	//if(!(key instanceof String)) return bones;
	if(!(values instanceof Array)) values = [values];
	for(var i = 0; i < arr.length; i++)
		if(anyType && values.indexOf(''+arr[i][key]) != -1) bones.push(arr[i]);
		else if(anyType && values.indexOf(0+arr[i][key]) != -1) bones.push(arr[i]);
		else if(values.indexOf(arr[i][key]) != -1) bones.push(arr[i]);
	return bones;
};

RecordArray.prototype.getName = function(id){
	var records = this.find('id',id)
	if(records.length == 0) return false;
	else if(records.length > 0) return records[0].name;
};

// RecordArray.prototype.clone = function(){
// 	var arr = this; // 
// 	var clone = [];
// 	for(var i = 0; i < arr.length; i++)
// 		clone.push( Object.assign({}, arr[i]) );
// 	return clone;
// };

// Find the index of a record using it's "id"
RecordArray.prototype.indexFrom = function(field, value, strict){
	// Assert field not null
	if(field==null) return;
	// Assert field is a string
	if(typeof(field) != 'string') throw new TypeError('Field must be a string');
	// Assert field exists in the first record
	if(this[field]===undefined) throw new TypeError('First record does not have field');

	// Iterate index "i" for this recordarray
	for(let i=0;i<this.length;i++)
		// If parameter strict is truthy then enforce type comparison
		if(this[i][field] == value && (!strict || this[i][field] === value))
			// exit function returning id as soon as found
			return i;
};

// Find the index of a record using it's "id"
RecordArray.prototype.indexFromID = function(id, strict){
	if(id==null) throw new TypeError('ID cannot be null or undefined');
	return RecordArray.prototype.call(this, 'id', id, strict);
};

// Find the index of a record using it's "tag"
RecordArray.prototype.indexFromTag = function(tag, strict){
	if(tag==null) throw new TypeError('Tag cannot be null or undefined');
	return RecordArray.prototype.call(this, 'tag', tag, strict);
};

RecordArray.prototype.unique = function(field, strict){
	// Default field to 'id'
	if(field == null) field = 'id';
	// Compare current index with index of first occurence of record with field with that value)
	return this.filter((e,i)=>this.indexFrom(field, e[field], strict)==i);
};

RecordArray.prototype.uniqueFrom = function(field, strict){
	// Assert field is a string
	if(field == null || typeof(field) != 'string') throw new TypeError('Field required');
	// Compare current index with index of first occurence of record with field with that value)
	return this.filter((e,i)=>this.indexFrom(field, e[field], strict)==i);
};


RecordArray.prototype.extend = function(arr){
	Object.assign(this, arr);
	return this;
};

/*
 * @description: Comparing 2 RecordArrays
 * @author: Francis Carelse
 * @param RA1: RecordArray
 * @param RA2: RecordArray
 * @param strict: Boolean will enforce second RecordArray only has the same records
 * @param identical: Boolean will enforce each record by index is compared
 * @returns: Boolean true if equal
 * @note: 
 */
RecordArray.compare = (RA1, RA2, strict, identical)=>{
	// Assert RA1 is an Array
	if(!(RA1 instanceof Array)) throw new TypeError('Parameter 1 must be Array or RecordArray');
	// Assert RA2 is an Array
	if(!(RA2 instanceof Array)) throw new TypeError('Parameter 2 must be Array or RecordArray');

	// Default "strict" to true
	if(strict !== false) strict = true;
	
	// Compare Lengths of unique IDs.
	if(strict && RA1.uniqueIDs.length !== RA2.uniqueIDs.length) return false;
	
	// Compare records
	if(identical){
		if(!RA1.every((record, index) => Records.compare(record, RA2[index], strict))) return false;
	} else {
		if(!RA1.every(record => Records.compare(record, RA2.findOne('id',record.id), strict))) return false;
	}

	return true;
};



