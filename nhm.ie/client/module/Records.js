
import * as Schema from './Schema.js'
import * as Filter from './Filter.js'

const Records = {
  // Compare fields in record 1 with record 2.
  // Strict comparison checks for matching set of keys
  compare:(record1, record2, strict)=>{
    // Default "strict" to true
    if(strict !== false) strict = true;
    
    // Compare Keys
    let keys1 = Object.keys(record1).sort();
    let keys2 = Object.keys(record2).sort();
    if(strict && keys1.length !== keys2.length) return false;
    
    // Compare keys
    if(!keys1.every((value, index) => value === keys2[index])) return false;
  
    // Compare values
    if(!keys1.every((key, index) => record1[key] === record2[key])) return false;
    return true;
  },

  // Compare ensuring strict comparison
  compareStrict: (rec1, rec2)=>{
    return compareRecords(rec1, rec2, true)
  },

  // Compare ensuring non strict comparison (rec2 can have extra fields)
  compareMinimal: (rec1, rec2)=>{
    return compareRecords(rec1, rec2, false)
  },
};

Records.filterMatch = (filter, record, type)=>{
};

Records.filterMatch = (filter, record, type)=>{
	if(filter == null || !(filter instanceof Object))
		throw new TypeError('Parameter 1: object required');
	if(record == null || !(record instanceof Object))
		throw new TypeError('Parameter 2: object required');
	if(type == null || !(typeof(type) == 'string'))
		throw new TypeError('Parameter 3: string required');
	if(!Schema.isType(type))
		throw new TypeError('Parameter 3: schema typestring required');
	if(filter instanceof Array)
		return Records.filterMatchAll(filter, record, type);
	var schema = Schema(type);
	return Filter(filter).match(record, schema);
};

Records.filterMatchAny = (filters, record, type)=>{
	if(filters == null || !(filters instanceof Array))
		throw new TypeError('Parameter 1: array required');
	if(record == null || !(record instanceof Object))
		throw new TypeError('Parameter 2: object required');
	if(type == null || !(typeof(type) == 'string'))
		throw new TypeError('Parameter 3: string required');
	return 0 < filters.filter(filter=>{
		if(filter instanceof Array)
			return Records.filterMatchAll(filter, record, type);
		else
			return Records.filterMatch(filter, record, type);
	}).length;
};

Records.filterMatchAll = (filters, record, type)=>{
	if(filters == null || !(filters instanceof Array))
		throw new TypeError('Parameter 1: array required');
	if(record == null || !(record instanceof Object))
		throw new TypeError('Parameter 2: object required');
	if(type == null || !(typeof(type) == 'string'))
		throw new TypeError('Parameter 3: string required');
	return filters.length == filters.filter(filter=>{
		if(filter instanceof Array)
			return Records.filterMatchAny(filter, record, type);
		else
			return Records.filterMatch(filter, record, type);
	}).length;
};

export default Records;
