/* eslint-disable */
/* global nhm */
const cuid = require('../lib/cuid/index.js')
import {fetchData as get} from './data.js'
const ID = nhm.ID = cuid();

const data = nhm.Global.data
const Notify = nhm.Global.Notify = {list: []};

Notify.add = function(note, type, delay){
	if(!delay) delay = 10000;
	if(!note) return;
	var prev = Util.findRecord(data.notices, 'note', note);
	var newNotice = {
		note: note,
		type: type || 'warning',
		id: noticeIdIterator++,
		created: new Date(),
		count: prev? prev.count + 1: 1
	};
	if(prev) Notify.close(prev.id);
	data.notices.push(newNotice);
	newNotice.timer = setTimeout(function(){
		Notify.close(newNotice.id);
		safeApply();
	}, delay);
	safeApply();
	return newNotice.id;
};

Notify.clear = function(){
	data.notices.forEach(function(notice){
		clearTimeout(notice.timer);
	});
	data.notices.length = 0; // Use Splice for Vue
};

Notify.close = function(id){
	if(id === undefined || id === null) return;
	for(var i = 0; i < data.notices.length; i++)
		if(id.toString() === data.notices[i].id.toString()){
			for(var j = i + 1; j < data.notices.length; j++)
				data.notices[j - 1] = data.notices[j];
			data.notices.length--;
		}
	safeApply();
};

export default Notify