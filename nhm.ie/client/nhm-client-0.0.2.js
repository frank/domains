var nhmIIFE2 = (function (sha256, moment) {
  'use strict';

  sha256 = sha256 && sha256.hasOwnProperty('default') ? sha256['default'] : sha256;
  moment = moment && moment.hasOwnProperty('default') ? moment['default'] : moment;

  var version = "0.0.2";

  /* eslint-disable */

  if(!window.nhm) window.nhm = {};
  window.fxc = window.nhm;
  if(!window.nhm.Global) window.nhm.Global = {};
  window.nhm.Global.version = version;

  const Global = nhm.Global;
  Global.config = {
  	tag: 'default',
  	name: 'Default Config',
  	data:{
  		uri: '/data',
  		type: 'json',
  		method: 'POST',
  	},
  	log: '/log',
  	auth: '/user',
  	email: false,
  	comms: false,
  };

  const Config = Global.Config = {};
  Config.configs = Global.configs = Global.configs==null? []: Global.configs;
  Config.add = (config)=>{
  	if(!config || !config.data) return false;
  	let configs = Config.configs.filter(conf=>{return conf.tag == config.tag});
  	if(configs.length<1)
  		Config.configs.push(config);
  	return true
  };
  Config.use = (tag)=>{
  	let config = Config.configs.filter(config=>{return config.tag == tag});
  	Global.config = config[0] || Global.config || Global.configs[0];
  };

  Config.remove = (tag)=>{
  	if(tag == 'default')
  		throw new Error('Cannot remove default config');
  	if(Global.config.tag == tag)
  		throw new Error('Cannot remove current config');
  	return Config.configs.remove('tag',tag);
  };

  var c = 0,
  	blockSize = 4,
  	base = 36,
  	discreteValues = Math.pow(base, blockSize),

  	pad = function pad(num, size) {
  		var s = "000000000" + num;
  		return s.substr(s.length-size);
  	},

  	randomBlock = function randomBlock() {
  		return pad((Math.random() *
  					discreteValues << 0)
  					.toString(base), blockSize);
  	},

  	safeCounter = function () {
  		c = (c < discreteValues) ? c : 0;
  		c++; // this is not subliminal
  		return c - 1;
  	},

  	api = function cuid() {
  		// Starting with a lowercase letter makes
  		// it HTML element ID friendly.
  		var letter = 'c', // hard-coded allows for sequential access

  			// timestamp
  			// warning: this exposes the exact date and time
  			// that the uid was created.
  			timestamp = (new Date().getTime()).toString(base),

  			// Prevent same-machine collisions.
  			counter,

  			// A few chars to generate distinct ids for different
  			// clients (so different computers are far less
  			// likely to generate the same id)
  			fingerprint = api.fingerprint(),

  			// Grab some more chars from Math.random()
  			random = randomBlock() + randomBlock();

  			counter = pad(safeCounter().toString(base), blockSize);

  		return  (letter + timestamp + counter + fingerprint + random);
  	};

  api.slug = function slug() {
  	var date = new Date().getTime().toString(36),
  		counter,
  		print = api.fingerprint().slice(0,1) +
  			api.fingerprint().slice(-1),
  		random = randomBlock().slice(-2);

  		counter = safeCounter().toString(36).slice(-4);

  	return date.slice(-2) +
  		counter + print + random;
  };

  api.globalCount = function globalCount() {
  	// We want to cache the results of this
  	var cache = (function calc() {
  			var i,
  				count = 0;

  			for (i in window) {
  				count++;
  			}

  			return count;
  		}());

  	api.globalCount = function () { return cache; };
  	return cache;
  };

  api.fingerprint = function browserPrint() {
  	return pad((navigator.mimeTypes.length +
  		navigator.userAgent.length).toString(36) +
  		api.globalCount().toString(36), 4);
  };

  /* eslint-disable */

  const DAO = function(TYPE, options){
  	if(!(DAO.types instanceof Object) || DAO.types==null) DAO.types = {};
    if(!options){
  		if(DAO.types[TYPE]) return DAO.types[TYPE];
  		options = {savedType: true};
  	}	const singular = options.singular || 'record'; // Override the name of the singular record paramater
  	const plural = options.plural || 'records'; // Override the name of the plural list paramater
  	const data = options.data instanceof Object && options.data !=null? options.data: {}; // Set where the data is written to.
  	const dao = DAO.types[TYPE] = {
  		reload(options){
  			return new Promise((resolve, reject)=>{
  				if(!(options instanceof Object) || options==null) options = {};
  				if(options.filters){
  					dao.filters = options.filters;
  				} else if(dao.filters){
  					options.filters = dao.filters;
  				} else options.filters = [];
  				const schema = nhm.Select.types[TYPE];
  				if(
  					schema && schema.meta && schema.meta.restrict &&
  					schema.meta.restrict.list == 'owner' &&
  					schema.meta.owner && nhm.Global.user.id && nhm.Global.user.type != 'admin'
  				){
  					options.filters.push({field: schema.meta.owner, value: nhm.Global.user.id});
  					schema.meta.owner;
  				}
  				nhm.Data(TYPE)
  				.list(options)
  				.then(records=>{
  					records = records.map(nhm.Data(TYPE).unSerial);
  					data[plural] = dao.records = records;
  					dao.watchers[plural].forEach(f=>f.run(records));
  					resolve(records);
  				})
  				.catch(nhm.Log.genLogger(`nhm.DAO.${TYPE}.${plural}.load`))
  				.catch(reject);
  			});
  		},
  		load(id){
  			// if(dao.loading){
  			// 	if(dao.loading == 2)
  			// 		dao.loadPromise = dao.loadPromise.then(()=>dao.load(id))
  			// 	dao.loading = 2;
  			// 	return dao.loadPromise;
  			// }
  			dao.loading = 1;
  			if(id==null){
  				if(data[singular])
  					id = data[singular].id;
  				else
  					return console.log('ID required to load')
  			}
  			dao.loadPromise = new Promise((resolve, reject)=>{
  				nhm.Data(TYPE).read({id:id})
  				.then(record=>{
  					record = nhm.Data(TYPE).unSerial(record);
  					data[singular] = dao.record = record;
  					dao.watchers[singular].forEach(f=>f.run(record));
  					nhm.Util.nextTick($=>resolve(record));
  				}).catch(err=>{
  					nhm.Log.genLogger(`nhm.DAO.${TYPE}.${singular}.load`);
  					console.error(err);
  					nhm.Util.nextTick($=>resolve(err));
  				}).finally(()=>{
  					if(dao.loading == 2) nhm.Util.nextTick(()=>dao.load(id));
  					dao.loading = 0;
  				});
  			});
  			return dao.loadPromise;
  		},
  		records: null,
  		record: null,
  		watchers: {[plural]:[],[singular]:[]},
  		watch(tag, watcher){
  			if(Object.keys(dao.watchers).indexOf(tag)==-1) throw Error('Invalid tag');
  			let id = api();
  			const watch = {id, run: watcher};
  			dao.watchers[tag].push(watch);
  			return watch.id;
  		},
  		unwatch(id){
  			Object.keys(dao.watchers).forEach(tag=>
  				nhm.Util.removeRecords(dao.watchers[tag], 'id', id)
  			);
  		},
  		changed(field){
  			const record = nhm.Data(TYPE).toSerial(data[singular]);
  			return nhm.Data(TYPE).change({
  				id: record.id,
  				field: field,
  				value: record[field]
  			})
  		}
  	};

  	if(options.watching) nhm.Comms.watch({type: TYPE},event=>{
  		switch(event.method){
  			case 'update': case 'change':	dao.load(); break;
  			case 'create': case 'delete': dao.reload();
  		}
  	});

  	return dao;
  };

  /* eslint-disable */
  if(!window.nhm) window.nhm = {};
  window.fxc = window.nhm;

  /**
   * Data function
   * CRUD object builder
   * options:
   * - singleton: do not allow another instance of this CRUB object to be created. Always return first creation.
   * - singular: tag for watching record updates
   * - plural: tag for watching list updates
   * - group: TBD: tag for watching selection updates (Not implemented yet.)
   * @param {string} TYPE The tag name of the schema to use on the server side.
   * @param {string} options Additional configuration parameters
   * @returns {object} CRUD object with crud functions to access the database
   */
  const Data = nhm.Data = function(TYPE, options){
  	// // Singleton parameter ensures CRUD object reused. Fixing options.
  	// if(Data.types && Data.types[TYPE] && Data.types[TYPE].options && Data.types[TYPE].options.singleton){
  	// 	return Data.types[TYPE];
  	// }
  	// If options given or 'savedType' set in options then new DAO object created
    if(!options){
  		if(!(Data.types instanceof Object) || Data.types==null){
  			Data.types = {};
  			options = {savedType: true};
  		}
  		if(Data.types[TYPE]) return Data.types[TYPE];
  	}	if(!(options instanceof Object) || options==null) options = {};
    Object.assign(options, {_csrf: nhm._csrf});
  	// if(Data.types[TYPE]) return Data.types[TYPE];
  	let Type;
    return Data.types[TYPE] = Type = {
  		options,
      create: (record)=>{
        return new Promise((resolve, reject)=>{
          const config = nhm.Global.config.data;
          if(!(Data.creates instanceof Array)) Data.creates = [];
          var create = {record, resolve, reject};
          Data.creates.push(create);
          if(Data.creates.length == 1) doNext();

          async function doCreate(record){
  					record = Type.toSerial(record);
            return await fetchData(config.uri, Object.assign({}, options, {record},
              {TYPE, METHOD: 'create'}))
          }

          function doNext(){
            const create = Data.creates[0];
            if(create) doCreate(create.record).then(res=>{
              Data.creates.shift();
              create.resolve(res);
              nhm.Util.nextTick(doNext);
            }).catch(err=>{
              Data.creates.shift();
              create.reject(err);
              nhm.Util.nextTick(doNext);
            });
          }
        })
      },

      update: (record)=>{
        return new Promise((resolve, reject)=>{
          const config = nhm.Global.config.data;
          if(!(Data.updates instanceof Array)) Data.updates = [];
          var update = {record, resolve, reject};
          Data.updates.push(update);
          if(Data.updates.length == 1) doNext();

          async function doUpdate(record){
  					record = Type.toSerial(record);
            return await fetchData(config.uri, Object.assign({}, options, record,
              {TYPE, METHOD: 'update'}))
          }

          function doNext(){
            const update = Data.updates[0];
            if(update) doUpdate(update.record).then(res=>{
              Data.updates.shift();
              update.resolve(res);
              nhm.Util.nextTick(doNext);
            }).catch(err=>{
              Data.updates.shift();
              update.reject(err);
              nhm.Util.nextTick(doNext);
            });
          }
        })
      },

      change: (change)=>{
        return new Promise((resolve, reject)=>{
          const config = nhm.Global.config.data;
          if(!(Data.changes instanceof Array)) Data.changes = [];
          var query = {change, resolve, reject};
          Data.changes.push(query);
          if(Data.changes.length == 1) doNext();

          async function doChange(change){
  					change.value = Type.toSerialField(change.field, change.value);
            return await fetchData(config.uri, Object.assign({}, options, change,
              {TYPE, METHOD: 'change'}))
          }

          function doNext(){
            const query = Data.changes[0];
            if(query) doChange(query.change).then(res=>{
              Data.changes.shift();
              query.resolve(res);
              nhm.Util.nextTick(doNext);
            }).catch(err=>{
              Data.changes.shift();
              query.reject(err);
              nhm.Util.nextTick(doNext);
            });
          }
        })
      },

      delete: async (id)=>{
        const config = nhm.Global.config.data;
        return await fetchData(config.uri, Object.assign({}, options,
          {TYPE, METHOD: 'delete', id}));
      },
      read: async (data)=>{
        const config = nhm.Global.config.data;
        const ret = await fetchData(config.uri, Object.assign({}, options, data,
          {TYPE, METHOD: 'read', data}));
  			return ret.error?ret:Type.unSerial(ret);
      },
      list: async (data)=>{
        const config = nhm.Global.config.data;
        const ret = await fetchData(config.uri, Object.assign({}, options, data,
          {TYPE, METHOD: 'list'}));
  			return ret.error?ret:ret.map(Type.unSerial);
      },
      count: async (data)=>{
        const config = nhm.Global.config.data;
        return await fetchData(config.uri, Object.assign({}, options,
          {TYPE, METHOD: 'count', data}));
      },
  		toSerial: record=>{
  			var serial = Object.assign({},record);
  			if(!nhm.Select || !nhm.Select.types || !nhm.Select.types[TYPE] ||
  			!(nhm.Select.types[TYPE].model instanceof Object)) return serial;
  			Object.keys(record).forEach(field=>{
  				const Field = nhm.Select.types[TYPE].model[field];
  				if(Field) switch((Field.type || 'none').toLowerCase()){
  					case 'jsonobject':{
  						serial[field] = nhm.Util.objectToJson(serial[field]);
  					} break;
  					case 'jsonarray':{
  						serial[field] = nhm.Util.arrayToJson(serial[field]);
  					} break;
  				}
  			});
  			return serial;
  		},
  		unSerial: serial=>{
  			var record = Object.assign({},record);
  			if(!nhm.Select || !nhm.Select.types || !nhm.Select.types[TYPE] ||
  			!(nhm.Select.types[TYPE].model instanceof Object)) return record;
  			Object.keys(record).forEach(field=>{
  				const Field = nhm.Select.types[TYPE].model[field];
  				if(Field) switch((Field.type || 'none').toLowerCase()){
  					case 'jsonobject': case 'object':{
  						record[field] = nhm.Util.jsonToObject(record[field]);
  					} break;
  					case 'jsonarray': case 'array':{
  						record[field] = nhm.Util.jsonToArray(record[field]);
  					} break;
  				}
  			});
  			return serial;
  		},
  		toSerialField: (field, value)=>{
  			if(
  				!nhm.Select || !nhm.Select.types || !nhm.Select.types[TYPE] ||
  				!nhm.Select.types[TYPE].model || !nhm.Select.types[TYPE].model[field]
  			) return value;
  			const Field = nhm.Select.types[TYPE].model[field];
  			if(Field) switch((Field.type || 'none').toLowerCase()){
  				case 'jsonobject': return nhm.Util.objectToJson(value);
  				case 'jsonarray': return nhm.Util.arrayToJson(value);
  			}
  			return value;
  		},
  		unSerialField: (field, value)=>{
  			if(
  				!nhm.Select || !nhm.Select.types || !nhm.Select.types[TYPE] ||
  				!nhm.Select.types[TYPE].model || !nhm.Select.types[TYPE].model[field]
  			) return value;
  			const Field = nhm.Select.types[TYPE].model[field];
  			if(Field) switch((Field.type || 'none').toLowerCase()){
  				case 'jsonobject': return nhm.Util.jsonToObject(value);
  				case 'jsonarray': return nhm.Util.jsonToArray(value);
  			}
  			return value;
  		}
    };
  };

  const fetchData = Data.fetchData = async (url = ``, data = {}, method = null)=>{
  	const config = nhm.Global.config.data;
    // Default options are marked with *
  	const res = await fetch(url, {
  			method: method || config.method || 'post', // *GET, POST, PUT, DELETE, etc.
  			mode: "cors", // no-cors, cors, *same-origin
  			cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
  			// credentials: "same-origin", // include, *same-origin, omit
  			headers: {
  					"Content-Type": "application/json; charset=utf-8",
  					// "Content-Type": "application/x-www-form-urlencoded",
  			},
  			// redirect: "follow", // manual, *follow, error
  			// referrer: "no-referrer", // no-referrer, *client
  			body: JSON.stringify(data), // body data type must match "Content-Type" header
  	});
  	const contentType = res.headers.get('content-type');
  	try{
  		return await res.json();
  	} catch(e){
  		// throw new Error('Non JSON data returned');
  		return {error: 500, message: e.message? e.message: e};
  	}
  };

  const post = Data.post = Data.fetchData;
  const get = Data.get = (url, data, method = 'GET') => Data.fetchData(url, data, method);

  /* eslint-disable */
  /* global nhm */
  const defaultUser = {type: 'guest', name: 'guest', id: 0};

  // if(!window.nhm) window.nhm = {};
  // if(!nhm.Global) nhm.Global = {};
  // if(!nhm.Global.config) nhm.Global.config = {};
  // const config = nhm.Global.config;

  const User = nhm.User = {user: nhm.Global.user || defaultUser};
  // User.events = new EventEmitter();

  User.watchers = {user:[]};

  User.nextWatcherID = 1;
  User.watch=(tag, watcher)=>{
  	if(Object.keys(User.watchers).indexOf(tag)==-1) throw Error('Invalid tag');
  	User.watchers[tag].push({id: User.nextWatcherID, run: watcher});
  	return User.nextWatcherID++;
  };
  User.unwatch=(id)=>{
  	Object.keys(User.watchers).forEach(tag=>
  		nhm.Util.removeRecords(User.watchers[tag], 'id', id)
  	);
  };

  User.getUser = async () => {
  	try{
  		User.lastError = null;
  		if(!nhm.Global.config || !nhm.Global.config.auth){
  			console.log(nhm.Global);
  			throw new Error('Config not initialized for auth');
  		}
  		const url = nhm.Global.config.auth+'/data.json';
  		// const res = await fetch(url, {
  		// 	method: nhm.Global.config.method || 'POST',
  		// 	mode: 'cors',
  		// 	cache: "no-cache",
  		// 	headers:{ "Content-Type": "application/json; charset=utf-8" },
  		// })
  		// const {user} = nhm.Global.session = await res.json();

  		let {user, types, global} = nhm.Global.session = await nhm.Data.fetchData(url,{},'post');

  		if(!user || !user.id){
  			user = Object.assign({}, defaultUser);
  			nhm.Global.session = {user};
  		}
  		//if(types) nhm.Select.types = types;
  		if(global && global.appTitle) nhm.Global.appTitle = global.appTitle;
  		nhm.Global.user = User.user = user;
  		User.watchers.user.forEach(f=>f.run(user));
  		return user;
  	}catch(err){
  		User.lastError = err;
  		console.error(err);
  		return User.user = {...defaultUser};
  	}
  };

  setTimeout(()=>{
  	User.getUser().catch(nhm.Log.genLogger('User.init.getUser'));
  },1);

  User.login = async credentials => {
  	if(!(nhm.Global.navTo instanceof Function))
  		nhm.Global.navTo = ()=>(()=>{});
  	User.lastError = null;
  	try{
  		const res = await nhm.Data.fetchData(
  			nhm.Global.config.auth+'/login',
  			{...credentials, 'returnJson': true}
  		);
  		if(res.error){
  			alert(res.message);
  			return User.logout().then(nhm.Global.navTo('/user/login'));
  		}
  		return await User.getUser().then(nhm.Global.navTo('/user/welcome'));
  	}catch(err){
  		User.lastError = err;
  		console.error(err);
  		return defaultUser;
  	}
  };

  User.logout = async () => {
  	try{
  		const res = await fetch(nhm.Global.config.auth+'/logout', {
  			method: 'POST',
  			mode: 'cors',
  			cache: "no-cache",
  			headers:{ "Content-Type": "application/json; charset=utf-8" },
  			body: JSON.stringify({return: true})
  		});
  		User.lastError = null;
  		return await User.getUser();
  	}catch(err){
  		User.lastError = err;
  		console.error(err);
  		return defaultUser;
  	}
  };

  User.register = async user => {
  	if(!(nhm.Global.navTo instanceof Function))
  		nhm.Global.navTo = ()=>(()=>{});
  	User.lastError = null;
  	try{
  		const res = await nhm.Data.fetchData(
  			nhm.Global.config.auth+'/register',
  			{...user, 'returnJson': true}
  		);
  		if(res.error){
  			alert(res.message);
  			return User.logout().then(nhm.Global.navTo('/user/register'));
  		}
  		return await User.getUser().then(nhm.Global.navTo('/user/welcome'));
  	}catch(err){
  		User.lastError = err;
  		console.error(err);
  		return defaultUser;
  	}
  };
  // export const fetchData = Data.fetchData = (url = ``, data = {})=>{
  //   // Default options are marked with *
  //     return fetch(url, {
  //         method: 'post' || config.method, // *GET, POST, PUT, DELETE, etc.
  //         mode: "cors", // no-cors, cors, *same-origin
  //         cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
  //         // credentials: "same-origin", // include, *same-origin, omit
  //         headers: {
  //             "Content-Type": "application/json; charset=utf-8",
  //             // "Content-Type": "application/x-www-form-urlencoded",
  //         },
  //         // redirect: "follow", // manual, *follow, error
  //         // referrer: "no-referrer", // no-referrer, *client
  //         body: JSON.stringify(data), // body data type must match "Content-Type" header
  //     })
  //     .then(res => {
  //       const contentType = res.headers.get('content-type');
  //       if(contentType && contentType.indexOf('application/json') !== -1)
  //         return res.json();
  //       else
  //         throw new Error('Non JSON data returned');
  //     }); // parses response to JSON
  // }

  /* eslint-disable */
  const ID = nhm.ID = api();

  const Comms = {readys: [], watching: [], watchers: []};

  Comms.ready = function(listener){
  	if(listener instanceof Function) Comms.readys.push(listener);
  	return new Promise(resolve=>Comms.readys.push(resolve))
  };

  Comms.init = async () => {
  	if(Comms.inited) return;
  	Comms.inited = true;
  	const confirmation = await fetchData('/comms/init', {terminalID: ID, watchers: Comms.watchers});
  	if(confirmation.success === 201){
  		Comms.readys.forEach(listener=>listener({...confirmation, ID, terminalID: ID}));
  		Comms.readys.length = 0;
  	} else {
  		// nhm.Notify.msg('Failed to initialize Comms Module','warning')
  		nhm.Log.error('Failed to initialize Comms Module', {ID, confirmation});
  	}
  	nhm.Util.nextTick(()=>Comms.wait());
  	return true;
  };

  Comms.wait = ()=>{
  	fetchData('/comms/wait', {terminalID: nhm.ID, watchers: Comms.watchers}).then(events=>{
  		if(events.error == 404) return fetchData('/comms/init', {terminalID: ID, watchers: Comms.watchers});
  		if(events.error == 408) return setTimeout(()=>Comms.wait(),2000);
  		if(events.error || !(events instanceof Array)) return setTimeout(()=>Comms.wait(),4000);
  		events.forEach(event=>
  			Comms.watching.forEach(watch=>{
  				if(event.type == watch.type &&
  					(!watch.method || event.method == watch.method) &&
  					(!watch.id || event.id == watch.id) &&
  					(!watch.tag || event.tag == watch.tag) &&
  					(!watch.field || event.field == watch.field)
  				) watch.watcher(event);
  		}));
  		setTimeout(()=>Comms.wait(),100);
  	})
  	.catch(err=>{
  		console.log(err);
  		if(err.statusCode == 500) return setTimeout(()=>fetchData('/comms/init', {terminalID: ID, watchers: Comms.watchers}),2000);
  		setTimeout(()=>Comms.wait(),10000);
  	})
  	.finally(()=>{
  		
  	});

  };

  Comms.watch = async (matcher,watcher)=>{
  	await Comms.init();
  	if(!(matcher instanceof Object) || matcher == null) throw TypeError('matcher needs to be an object')
  	if(!(watcher instanceof Function)) throw TypeError('watcher needs to be a function')
  	const watch = {watcher, matcher, ...matcher, terminalID: nhm.ID};
  	// Stupid error about push not being a function when it actually is working
  	const ret = await fetchData('/comms/watch', watch);
  	if(!ret.error){
  		Comms.watching.push(watch);
  		Comms.watchers.push(ret);
  	} else console.log(ret);
  	// console.log(Comms.watching);
  	return watch;
  };

  var Select = {
  	// statuses: [{id:'AC',name:'Active'}]
  	types: {},
  };

  const Schema = function(type){
  	return {
  		convertValueToField: function(value, field){
  			if(value==null) return null;
  			switch(this.getType(field)){
  				case 'number': return isNaN(value)? parseFloat(value): value;
  				case 'string': default:
  					return value.toString();
  			}
  		}
  	};
  };

  // This really needs to be implemented to be useful.
  Schema.isType = type=>true;
  Schema.cleanTypeName = type=>typeof(type)=='string'?type.replace(/[^a-zA-Z]/,''):'';

  /**
   * Filtering system for FXC frameworks
   */
  // import * as Schema from './Schema.js';
  // var moment = require('moment');

  var Filter = function(proto){
  	var filter = Object.assign({field: 'id', op: 'eq', value: 0},proto);
  	filter.match = (record, schema)=>{
  		if(!(record instanceof Object)) throw new TypeError('Parameter "record" must be an object')
  		if(record[filter.field] === undefined) throw new TypeError(`Record's field "${filter.field}" not defined`)
  		var fieldType = schema.getfieldType(filter.field);
  		switch(filter.op){
  			case 'gt': // Greater than
  				return typeAdjust(record[filter.field], fieldType) > typeAdjust(filter.value, fieldType)
  			case 'lt': // Less than
  				return typeAdjust(record[filter.field], fieldType) < typeAdjust(filter.value, fieldType)
  			case 'ge': // Greater or equal
  				return typeAdjust(record[filter.field], fieldType) >= typeAdjust(filter.value, fieldType);
  			case 'le': // Less or equal
  				return typeAdjust(record[filter.field], fieldType) <= typeAdjust(filter.value, fieldType);
  			// // 5 Features to be added later
  			// case 'nu': // Null
  			// 	return record[filter.field] == null;
  			// case 'nn': // Not null
  			// 	return record[filter.field] != null;
  			// case 'li': // Like
  			// 	return typeAdjust(record[filter.field], fieldType) != typeAdjust(filter.value, fieldType);
  			// case 'sw': // Starts with
  			// 	return typeAdjust(record[filter.field], fieldType) != typeAdjust(filter.value, fieldType);
  			// case 'ew': // Ends with
  			// 	return typeAdjust(record[filter.field], fieldType) != typeAdjust(filter.value, fieldType);
  			case 'ne': // Not equal
  				return typeAdjust(record[filter.field], fieldType) != typeAdjust(filter.value, fieldType);
  			case 'eq': default: // Equal
  				return typeAdjust(record[filter.field], fieldType) == typeAdjust(filter.value, fieldType);
  		}
  	};

  	return filter;
  };

  function typeAdjust(value, fieldType){
  	switch(fieldType.toString().toLowerCase()){
  		case 'number': case 'money':
  			return value == null? null:
  				isNaN(value)?parseFloat(value):value;
  		case 'int': case 'integer':
  		case 'select': case 'selectid':
  			return value == null? null:
  				isNaN(value)?parseInt(value):value;
  	}
  	return value == null? null: ''+value;
  }
  Filter.typeAdjust = typeAdjust;

  const Records = {
    // Compare fields in record 1 with record 2.
    // Strict comparison checks for matching set of keys
    compare:(record1, record2, strict)=>{
      // Default "strict" to true
      if(strict !== false) strict = true;
      
      // Compare Keys
      let keys1 = Object.keys(record1).sort();
      let keys2 = Object.keys(record2).sort();
      if(strict && keys1.length !== keys2.length) return false;
      
      // Compare keys
      if(!keys1.every((value, index) => value === keys2[index])) return false;
    
      // Compare values
      if(!keys1.every((key, index) => record1[key] === record2[key])) return false;
      return true;
    },

    // Compare ensuring strict comparison
    compareStrict: (rec1, rec2)=>{
      return compareRecords(rec1, rec2, true)
    },

    // Compare ensuring non strict comparison (rec2 can have extra fields)
    compareMinimal: (rec1, rec2)=>{
      return compareRecords(rec1, rec2, false)
    },
  };

  Records.filterMatch = (filter, record, type)=>{
  };

  Records.filterMatch = (filter, record, type)=>{
  	if(filter == null || !(filter instanceof Object))
  		throw new TypeError('Parameter 1: object required');
  	if(record == null || !(record instanceof Object))
  		throw new TypeError('Parameter 2: object required');
  	if(type == null || !(typeof(type) == 'string'))
  		throw new TypeError('Parameter 3: string required');
  	if(!Schema.isType(type))
  		throw new TypeError('Parameter 3: schema typestring required');
  	if(filter instanceof Array)
  		return Records.filterMatchAll(filter, record, type);
  	var schema = Schema();
  	return Filter(filter).match(record, schema);
  };

  Records.filterMatchAny = (filters, record, type)=>{
  	if(filters == null || !(filters instanceof Array))
  		throw new TypeError('Parameter 1: array required');
  	if(record == null || !(record instanceof Object))
  		throw new TypeError('Parameter 2: object required');
  	if(type == null || !(typeof(type) == 'string'))
  		throw new TypeError('Parameter 3: string required');
  	return 0 < filters.filter(filter=>{
  		if(filter instanceof Array)
  			return Records.filterMatchAll(filter, record, type);
  		else
  			return Records.filterMatch(filter, record, type);
  	}).length;
  };

  Records.filterMatchAll = (filters, record, type)=>{
  	if(filters == null || !(filters instanceof Array))
  		throw new TypeError('Parameter 1: array required');
  	if(record == null || !(record instanceof Object))
  		throw new TypeError('Parameter 2: object required');
  	if(type == null || !(typeof(type) == 'string'))
  		throw new TypeError('Parameter 3: string required');
  	return filters.length == filters.filter(filter=>{
  		if(filter instanceof Array)
  			return Records.filterMatchAny(filter, record, type);
  		else
  			return Records.filterMatch(filter, record, type);
  	}).length;
  };

  /* RecordArray
   * Optional replacement for Array to operate as a array of records.
   * 
   * @author Francis Carelse
   * @version 0.4.3.23
   */

  /**
   * @description: An extension of Array that provides record processing related methods
   * @author 'Francis Carelse'
   */
  class RecordArray extends Array{
  	/**
  	 * @constructor
  	 * @param {Array} array 
  	 */
  	constructor(array){
  		super();
  		var _this = this;
  		// If supplied array parameter is an Array
  		if(array instanceof Array)
  			// Then iterate over each object in it
  			array.forEach(function(record){
  			// Push a duplicate of record
  	 		_this.push(Object.assign({},record));
  	 	});
  	}

  	findOne(key, value, strict){
  		var arr = this;
  		strict = !!strict;
  		if(value === null) return false;
  		if(value === undefined) return false;
  		for(var i = 0; i < arr.length; i++)
  			if(arr[i][key] && ((!strict && arr[i][key] == value) || arr[i][key] === value)) return arr[i];
  		return false;
  	};

  	findOneById(value, strict){
  		return this.findOne('id', value, strict)
  	}
  	
  	findOneByTag(value, strict){
  		return this.findOne('tag', value, strict)
  	}

  	sortBy(field, order){
  		// Assert field parameter is a string.
  		if(typeof(field)!='string')
  			throw new TypeError('String expected for first parameter.');
  		// Assert order parameter is ASC or DESC
  		if(typeof(order)!='string' || !(order.toLowerCase() == 'asc' || order.toLowerCase() == 'desc'))
  			throw new TypeError('\'ASC\' or \'DESC\' String expected for second parameter.');
  		//Return sorted using appropriate function
  		return this.sort((order.toLowerCase() == 'asc')?
  			sortFnASC:
  			sortFnDESC);
  		// Sorting Ascending Strategy
  		function sortFnASC(a,b){
  			var c =
  				// Evaluate to 0 f equal
  				(a[field]==b[field]?0:
  				// 1 indicates wrong order. -1 indicates correct order
  				(a[field]>b[field]?1:-1));
  			// This does not work if you do not assign to a variable before returning.
  			return c;
  		}
  		// Sorting Descending Strategy (just reverse the testing parameters)
  		function sortFnDESC(a,b){ return sortFnASC(b,a) }
  	}
  	
  	/**
  	 * Sort this RecordArray by a set of fields in ascending order
  	 * Takes an array of strings or a space separated string of fieldnames
  	 * @param {Array<String> | String} fields
  	 */
  	sortASC(fields){
  		// If fields parameter is not already an Array
  		if(!(fields instanceof Array))
  			// Ensure is string and split space separated fieldnames
  			fields = fields.toString().split(' ');
  		// Throw out any non string fields
  		fields = fields.filter(f=>typeof f == 'string');
  		// If no fields left then abort
  		if(!fields.length) throw new TypeError('Parameter "fields" needs to be an array of strings or space separated list of field names');
  		// Return sort using item pair evaluation strategy
  		return this.sort(function(a,b){
  			// Iterate over fields list
  			for(var i=0;i<fields.length;i++)
  				// Sequentially check for the first instance of inequality
  				if(a[fields[i]]!=b[fields[i]])
  					// If wrong order then pass back 1 otherwise -1
  					return a[fields[i]]>b[fields[i]]?1:-1;
  			// All fields are equal so return 0 for matching
  			return 0;
  		});
  	}

  	/**
  	 * Sort this RecordArray by a set of fields in descending order
  	 * Takes an array of strings or a space separated string of fieldnames
  	 * @param {Array<String> | String} fields
  	 */
  	sortDESC(fields){
  		// If fields parameter is not already an Array
  		if(!(fields instanceof Array))
  			// Ensure is string and split space separated fieldnames
  			fields = fields.toString().split(' ');
  		// Throw out any non string fields
  		fields = fields.filter(f=>typeof f == 'string');
  		// If no fields left then abort
  		if(!fields.length) throw new TypeError('Parameter "fields" needs to be an array of strings or space separated list of field names');
  		// Return sort using item pair evaluation strategy
  		return this.sort(function(a,b){
  			// Iterate over fields list
  			for(var i=0;i<fields.length;i++)
  				// Sequentially check for the first instance of inequality
  				if(a[fields[i]]!=b[fields[i]])
  					// If wrong order then pass back 1 otherwise -1
  					return a[fields[i]]<b[fields[i]]?1:-1;
  			// All fields are equal so return 0 for matching;
  			return 0;
  		});
  	}

  	/**
  	 * Clone this RecordArray or supplied Array of reords to a new RecordArray
  	 * @param {Array} arr 
  	 */
  	clone(arr){
  		// If no source array supplied then use this one
  		arr = arr || this;
  		// Create new RecordArray
  		var clone = new RecordArray();
  		for(var i = 0; i < arr.length; i++)
  			clone.push( Object.assign({}, arr[i]) );
  		return clone;
  	};

  	/**
  	 * @returns 'Array of cloned records'
  	 */
  	toArray(){
  		// Clone to an Array
  		return this.map(record=>Object.assign({},record));
  	}

  	// TBD
  	read(options, filters){
  		throw Error('Function yet to be developed')
  	}
  	list(options, filters){
  		throw Error('Function yet to be developed')
  	}
  	create(options, filters){
  		throw Error('Function yet to be developed')
  	}
  	update(options, filters){
  		throw Error('Function yet to be developed')
  	}
  	delete(options, filters){
  		throw Error('Function yet to be developed')
  	}
  	
  }

  RecordArray.prototype.find = function(key, values, anyType){
  	var arr = this;
  	var bones = [];
  	//if(!(key instanceof String)) return bones;
  	if(!(values instanceof Array)) values = [values];
  	for(var i = 0; i < arr.length; i++)
  		if(anyType && values.indexOf(''+arr[i][key]) != -1) bones.push(arr[i]);
  		else if(anyType && values.indexOf(0+arr[i][key]) != -1) bones.push(arr[i]);
  		else if(values.indexOf(arr[i][key]) != -1) bones.push(arr[i]);
  	return bones;
  };

  RecordArray.prototype.getName = function(id){
  	var records = this.find('id',id);
  	if(records.length == 0) return false;
  	else if(records.length > 0) return records[0].name;
  };

  // RecordArray.prototype.clone = function(){
  // 	var arr = this; // 
  // 	var clone = [];
  // 	for(var i = 0; i < arr.length; i++)
  // 		clone.push( Object.assign({}, arr[i]) );
  // 	return clone;
  // };

  // Find the index of a record using it's "id"
  RecordArray.prototype.indexFrom = function(field, value, strict){
  	// Assert field not null
  	if(field==null) return;
  	// Assert field is a string
  	if(typeof(field) != 'string') throw new TypeError('Field must be a string');
  	// Assert field exists in the first record
  	if(this[field]===undefined) throw new TypeError('First record does not have field');

  	// Iterate index "i" for this recordarray
  	for(let i=0;i<this.length;i++)
  		// If parameter strict is truthy then enforce type comparison
  		if(this[i][field] == value && (!strict || this[i][field] === value))
  			// exit function returning id as soon as found
  			return i;
  };

  // Find the index of a record using it's "id"
  RecordArray.prototype.indexFromID = function(id, strict){
  	if(id==null) throw new TypeError('ID cannot be null or undefined');
  	return RecordArray.prototype.call(this, 'id', id, strict);
  };

  // Find the index of a record using it's "tag"
  RecordArray.prototype.indexFromTag = function(tag, strict){
  	if(tag==null) throw new TypeError('Tag cannot be null or undefined');
  	return RecordArray.prototype.call(this, 'tag', tag, strict);
  };

  RecordArray.prototype.unique = function(field, strict){
  	// Default field to 'id'
  	if(field == null) field = 'id';
  	// Compare current index with index of first occurence of record with field with that value)
  	return this.filter((e,i)=>this.indexFrom(field, e[field], strict)==i);
  };

  RecordArray.prototype.uniqueFrom = function(field, strict){
  	// Assert field is a string
  	if(field == null || typeof(field) != 'string') throw new TypeError('Field required');
  	// Compare current index with index of first occurence of record with field with that value)
  	return this.filter((e,i)=>this.indexFrom(field, e[field], strict)==i);
  };


  RecordArray.prototype.extend = function(arr){
  	Object.assign(this, arr);
  	return this;
  };

  /*
   * @description: Comparing 2 RecordArrays
   * @author: Francis Carelse
   * @param RA1: RecordArray
   * @param RA2: RecordArray
   * @param strict: Boolean will enforce second RecordArray only has the same records
   * @param identical: Boolean will enforce each record by index is compared
   * @returns: Boolean true if equal
   * @note: 
   */
  RecordArray.compare = (RA1, RA2, strict, identical)=>{
  	// Assert RA1 is an Array
  	if(!(RA1 instanceof Array)) throw new TypeError('Parameter 1 must be Array or RecordArray');
  	// Assert RA2 is an Array
  	if(!(RA2 instanceof Array)) throw new TypeError('Parameter 2 must be Array or RecordArray');

  	// Default "strict" to true
  	if(strict !== false) strict = true;
  	
  	// Compare Lengths of unique IDs.
  	if(strict && RA1.uniqueIDs.length !== RA2.uniqueIDs.length) return false;
  	
  	// Compare records
  	if(identical){
  		if(!RA1.every((record, index) => Records.compare(record, RA2[index], strict))) return false;
  	} else {
  		if(!RA1.every(record => Records.compare(record, RA2.findOne('id',record.id), strict))) return false;
  	}

  	return true;
  };

  window.Base64 = {};

  (function(global) {
  	// existing version for noConflict()
  	var _Base64 = global.Base64;
  	var version = '2.1.9';
  	// if node.js, we use Buffer
  	var buffer;
  	if (typeof module !== 'undefined' && module.exports) {
  		try {
  			buffer = require('buffer').Buffer;
  		} catch (err) {}
  	}
  	// constants
  	var b64chars
  		= 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
  	var b64tab = function(bin) {
  		var t = {};
  		for (var i = 0, l = bin.length; i < l; i++) t[bin.charAt(i)] = i;
  		return t;
  	}(b64chars);
  	var fromCharCode = String.fromCharCode;
  	// encoder stuff
  	var cb_utob = function(c) {
  		var cc;
  		if (c.length < 2) {
  			cc = c.charCodeAt(0);
  			return cc < 0x80 ? c
  				: cc < 0x800 ? (fromCharCode(0xc0 | (cc >>> 6))
  								+ fromCharCode(0x80 | (cc & 0x3f)))
  				: (fromCharCode(0xe0 | ((cc >>> 12) & 0x0f))
  					+ fromCharCode(0x80 | ((cc >>>  6) & 0x3f))
  					+ fromCharCode(0x80 | ( cc		 & 0x3f)));
  		} else {
  			cc = 0x10000
  				+ (c.charCodeAt(0) - 0xD800) * 0x400
  				+ (c.charCodeAt(1) - 0xDC00);
  			return (fromCharCode(0xf0 | ((cc >>> 18) & 0x07))
  					+ fromCharCode(0x80 | ((cc >>> 12) & 0x3f))
  					+ fromCharCode(0x80 | ((cc >>>  6) & 0x3f))
  					+ fromCharCode(0x80 | ( cc		 & 0x3f)));
  		}
  	};
  	var re_utob = /[\uD800-\uDBFF][\uDC00-\uDFFFF]|[^\x00-\x7F]/g;
  	var utob = function(u) {
  		return u.replace(re_utob, cb_utob);
  	};
  	var cb_encode = function(ccc) {
  		var padlen = [0, 2, 1][ccc.length % 3],
  		ord = ccc.charCodeAt(0) << 16
  			| ((ccc.length > 1 ? ccc.charCodeAt(1) : 0) << 8)
  			| ((ccc.length > 2 ? ccc.charCodeAt(2) : 0)),
  		chars = [
  			b64chars.charAt( ord >>> 18),
  			b64chars.charAt((ord >>> 12) & 63),
  			padlen >= 2 ? '=' : b64chars.charAt((ord >>> 6) & 63),
  			padlen >= 1 ? '=' : b64chars.charAt(ord & 63)
  		];
  		return chars.join('');
  	};
  	var btoa = global.btoa ? function(b) {
  		return global.btoa(b);
  	} : function(b) {
  		return b.replace(/[\s\S]{1,3}/g, cb_encode);
  	};
  	var _encode = buffer ? function (u) {
  		return (u.constructor === buffer.constructor ? u : new buffer(u))
  		.toString('base64');
  	}
  	: function (u) { return btoa(utob(u)); }
  	;
  	var encode = function(u, urisafe) {
  		return !urisafe
  			? _encode(String(u))
  			: _encode(String(u)).replace(/[+\/]/g, function(m0) {
  				return m0 == '+' ? '-' : '_';
  			}).replace(/=/g, '');
  	};
  	var encodeURI = function(u) { return encode(u, true); };
  	// decoder stuff
  	var re_btou = new RegExp([
  		'[\xC0-\xDF][\x80-\xBF]',
  		'[\xE0-\xEF][\x80-\xBF]{2}',
  		'[\xF0-\xF7][\x80-\xBF]{3}'
  	].join('|'), 'g');
  	var cb_btou = function(cccc) {
  		switch(cccc.length) {
  		case 4:
  			var cp = ((0x07 & cccc.charCodeAt(0)) << 18)
  				|	((0x3f & cccc.charCodeAt(1)) << 12)
  				|	((0x3f & cccc.charCodeAt(2)) <<  6)
  				|	 (0x3f & cccc.charCodeAt(3)),
  			offset = cp - 0x10000;
  			return (fromCharCode((offset  >>> 10) + 0xD800)
  					+ fromCharCode((offset & 0x3FF) + 0xDC00));
  		case 3:
  			return fromCharCode(
  				((0x0f & cccc.charCodeAt(0)) << 12)
  					| ((0x3f & cccc.charCodeAt(1)) << 6)
  					|  (0x3f & cccc.charCodeAt(2))
  			);
  		default:
  			return  fromCharCode(
  				((0x1f & cccc.charCodeAt(0)) << 6)
  					|  (0x3f & cccc.charCodeAt(1))
  			);
  		}
  	};
  	var btou = function(b) {
  		return b.replace(re_btou, cb_btou);
  	};
  	var cb_decode = function(cccc) {
  		var len = cccc.length,
  		padlen = len % 4,
  		n = (len > 0 ? b64tab[cccc.charAt(0)] << 18 : 0)
  			| (len > 1 ? b64tab[cccc.charAt(1)] << 12 : 0)
  			| (len > 2 ? b64tab[cccc.charAt(2)] <<  6 : 0)
  			| (len > 3 ? b64tab[cccc.charAt(3)]	   : 0),
  		chars = [
  			fromCharCode( n >>> 16),
  			fromCharCode((n >>>  8) & 0xff),
  			fromCharCode( n		 & 0xff)
  		];
  		chars.length -= [0, 0, 2, 1][padlen];
  		return chars.join('');
  	};
  	var atob = global.atob ? function(a) {
  		return global.atob(a);
  	} : function(a){
  		return a.replace(/[\s\S]{1,4}/g, cb_decode);
  	};
  	var _decode = buffer ? function(a) {
  		return (a.constructor === buffer.constructor
  				? a : new buffer(a, 'base64')).toString();
  	}
  	: function(a) { return btou(atob(a)); };
  	var decode = function(a){
  		return _decode(
  			String(a).replace(/[-_]/g, function(m0) { return m0 == '-' ? '+' : '/'; })
  				.replace(/[^A-Za-z0-9\+\/]/g, '')
  		);
  	};
  	var noConflict = function() {
  		var Base64 = global.Base64;
  		global.Base64 = _Base64;
  		return Base64;
  	};
  	// export Base64
  	global.Base64 = {
  		VERSION: version,
  		atob: atob,
  		btoa: btoa,
  		fromBase64: decode,
  		toBase64: encode,
  		utob: utob,
  		encode: encode,
  		encodeURI: encodeURI,
  		btou: btou,
  		decode: decode,
  		noConflict: noConflict
  	};
  	// if ES5 is available, make Base64.extendString() available
  	if (typeof Object.defineProperty === 'function') {
  		var noEnum = function(v){
  			return {value:v,enumerable:false,writable:true,configurable:true};
  		};
  		global.Base64.extendString = function () {
  			Object.defineProperty(
  				String.prototype, 'fromBase64', noEnum(function () {
  					return decode(this);
  				}));
  			Object.defineProperty(
  				String.prototype, 'toBase64', noEnum(function (urisafe) {
  					return encode(this, urisafe);
  				}));
  			Object.defineProperty(
  				String.prototype, 'toBase64URI', noEnum(function () {
  					return encode(this, true);
  				}));
  		};
  	}
  	// that's it!
  	if (global['Meteor']) {
  		Base64 = global.Base64; // for normal export in Meteor.js
  	}
  })(window);

  var Base64$1 = window.Base64;

  const Util = {};
  Util.RecordArray = RecordArray;


  Util.Watch = function(types){
  	const Watch = {

  		// Add tag type
  		addType(type){
  			return Watch.watchers[type] = Watch.watchers[type] || [];
  		},

  		// Verify tag type
  		isType(type){
  			return Watch.watchers[type] instanceof Array;
  		},

  		// Collections of watches by tag
  		watchers: {},

  		/**
  		 * Set up a watcher for a specific tag
  		 * @param {string} tag 
  		 * @param {function} watcher
  		 * @returns {string} cuid ID of watch record for use with unwatch
  		 * @throws {Error}
  		 */
  		watch(tag, watcher){
  			if(!(tag instanceof String) || tag == null)
  				throw TypeError('tag string required')
  			if(Object.keys(Watch.watchers).indexOf(tag)==-1)
  				throw Error('Invalid tag');
  			if(!(watcher instanceof Object) || watcher == null)
  				throw TypeError('Watcher needs to be a function')
  			const watch = {id: api(), run: watcher};
  			Watch.watchers[tag].push(watch);
  			return watch.id;
  		},

  		/**
  		 * Removes watch record
  		 * @param {string} id cuid of watch record to unwatch
  		 */
  		unwatch(id){
  			if(!id) return;
  			Object.keys(Watch.watchers).forEach(tag=>
  				Util.removeRecords(Watch.watchers[tag], 'id', id)
  			);
  		},

  		/**
  		 * Alerts watchers of a tag with a value or values
  		 * @param {string} tag
  		 * @param {any} value
  		 */
  		emit(tag){
  			if(!(tag instanceof String) || tag == null)
  				throw TypeError('tag string required')
  			if(Object.keys(Watch.watchers).indexOf(tag)==-1)
  				throw Error('Invalid tag');
  			const args = Array.prototype.split.call(arguments);
  			args.shift();
  			if(args.length == 0) args.push(true);
  			Watch.watchers[tag].forEach(f=>f.run.apply(null, args));
  		}
  	};
  	if(types instanceof Array)
  		types.forEach(type=>Watch.addType(type));
  	return Watch;
  };

  Util.merge = function(arr){
  	if(!(arr instanceof Array)) return arr;
  	for(var i=1;i<arguments.length;i++){
  		if(arguments[i] instanceof Array)
  			for(var j=0;j<arguments[i].length;j++)
  				arr.push(arguments[i][j]);
  		else
  			arr.push(arguments[i]);
  	}
  	return arr;
  };

  Util.bind = function bind(func, target){
  	return function(){
  		func.apply(target, arguments);
  	};
  };

  Util.nextTick = callback => setTimeout(callback,0);

  Util.randomString = function (len, charSet) {
  	charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  	len = len || 8;
  	var randomString = '';
  	for (var i = 0; i < len; i++) {
  		var randomPoz = Math.floor(Math.random() * charSet.length);
  		randomString += charSet.substring(randomPoz,randomPoz+1);
  	}
  	return randomString;
  };

  Util.decodeHtmlEntity = function(str) {
  	return str.replace(/&#(\d+);/g, function(match, dec) {
  		return String.fromCharCode(dec);
  	});
  };

  Util.encodeHtmlEntity = function(str) {
  	var buf = [];
  	for (var i=str.length-1;i>=0;i--) {
  		buf.unshift(['&#', str[i].charCodeAt(), ';'].join(''));
  	}
  	return buf.join('');
  };

  Util.genFilter = function(f,o,v,g,i){return {field:f,op:o,value:v,group:g||null,id:i||null};};

  // Generate an ordering function depending on ordering required for a field and take a second set of field and ordering for second comparison.
  Util.compareFn = function(field, order, field2, order2){
  	order = order == 'DESC'? 'DESC': 'ASC';
  	return {
  		DESC: function(a,b){
  			var c=((a[field] instanceof String)?a[field].trim():a[field])+'';
  			var d=((b[field] instanceof String)?b[field].trim():b[field])+'';
  			if(isNaN(c) && isNaN(d))
  				return c > d? -1: c < d? 1: 
  					field2? Util.compareFn(field2, order2)(a,b): 0;
  			else
  				return d - c? d - c:
  					field2? Util.compareFn(field2, order2)(a,b): 0;

  		},
  		ASC: function(a,b){ 
  			var c=((a[field] instanceof String)?a[field].trim():a[field])+'';
  			var d=((b[field] instanceof String)?b[field].trim():b[field])+'';
  			if(isNaN(c) && isNaN(d))
  				return c < d? -1: c > d? 1: 
  					field2? Util.compareFn(field2, order2)(a,b): 0;
  			else
  				return c - d? c - d: 
  					field2? Util.compareFn(field2, order2)(a,b): 0;
  		},
  	}[order];
  };

  // Sort record by 2 sets of field and order.
  Util.sortRecords = Util.sortBones = function(arr, field, order, field2, order2, trim){
  	if(!(arr instanceof Array)) return false;
  	if(!field) return false;
  	if(order != 'DESC') order = 'ASC';
  	arr.sort(Util.compareFn(field, order, field2, order2, trim));
  	return arr;
  };

  Util.findRecord = Util.findBone = function(arr, key, value){
  	if(!(arr instanceof Array)) return false;
  	if(value === null) return false;
  	if(value === undefined) return false;
  	for(var i = 0; i < arr.length; i++)
  		if(arr[i][key] && arr[i][key].toString() === value.toString()) return arr[i];
  	return false;
  };

  Util.findRecords = Util.findBones = function(arr, key, values, anyType){
  	var bones = [];
  	if(!(arr instanceof Array)) return bones;
  	//if(!(key instanceof String)) return bones;
  	if(!(values instanceof Array)) values = [values];
  	for(var i = 0; i < arr.length; i++)
  		if(anyType && values.indexOf(''+arr[i][key]) != -1) bones.push(arr[i]);
  		else if(anyType && values.indexOf(0+arr[i][key]) != -1) bones.push(arr[i]);
  		else if(values.indexOf(arr[i][key]) != -1) bones.push(arr[i]);
  	return bones;
  };

  Util.matchRecords = Util.matchBones = function(arr, key, values){
  	var bones = [];
  	if(!(arr instanceof Array)) return bones;
  	//if(!(key instanceof String)) return bones;
  	if(values instanceof String || !(values instanceof Array)) values = [values];
  	values = [].concat(values);
  	for(var i = 0; i < values.length; i++)
  		bones[i] = Util.findRecord(arr, key, values[i]);
  	//if(values.indexOf(arr[i][key]) != -1) bones.push(arr[i]);
  	return bones;
  };

  Util.findRecordsWithFieldSet = function(arr, field){
  	var bones = [];
  	if(!(arr instanceof Array)) return bones;
  	//if(!(key instanceof String)) return bones;
  	if(field !== undefined && field !== null)
  		for(var i = 0; i < arr.length; i++)
  			if(arr[i][field] !== undefined && arr[i][field] !== null) bones.push(arr[i]);
  	return bones;
  };

  Util.cloneRecords = Util.cloneBones = function(arr){
  	if(!(arr instanceof Array)) return [];
  	var clone = [];
  	for(var i = 0; i < arr.length; i++)
  		clone.push( Object.assign({}, arr[i]) );
  	return clone;
  };

  Util.clearField = function(arr, field){
  	if(!field) return arr;
  	if(!(arr instanceof Array)) return [];
  	for(var i = 0; i < arr.length; i++)
  		if(arr[i][field] !== undefined) delete arr[i][field];
  	return arr;
  };

  Util.setField = function(arr, field, value, existingOnly){
  	if(!field) return arr;
  	if(!(arr instanceof Array)) return [];
  	for(var i = 0; i < arr.length; i++)
  		if(arr[i][field] !== undefined || !existingOnly) arr[i][field] = value;
  	return arr;
  };

  Util.listValues = function(arr, key){
  	var list = [];
  	for(var i = 0; i < arr.length; i++)
  		list.push(arr[i][key]);
  	return list;
  };

  Util.findValues = function(arr, key){
  	var list = [];
  	for(var i = 0; i < arr.length; i++)
  		if(arr[i][key] !== undefined && arr[i][key] !== null)
  			list.push(arr[i][key]);
  	return list;
  };

  Util.uniqueValues = function(arr, key){
  	var list = [];
  	for(var i = 0; i < arr.length; i++)
  		if(list.indexOf(arr[i][key])==-1 && arr[i][key] !== undefined && arr[i][key] !== null)
  			list.push(arr[i][key]);
  	return list;
  };

  Util.removeRecords = Util.removeBones = function(arr, key, values, anyType){
  	if(!(arr instanceof Array)) return 0;
  	var found = 0;
  	if(!(values instanceof Array)) values = [values];
  	for(var i = 0; i < arr.length; i++)
  		if((anyType && values.indexOf(''+arr[i][key]) != -1) ||
  			(anyType && values.indexOf(0+arr[i][key]) != -1) ||
  			(values.indexOf(arr[i][key]) != -1)){
  			for(var j = i--; j < arr.length; j++)
  				arr[j] = arr[j + 1];
  			arr.length--;
  			found++;
  		}
  	return found;
  };

  Util.copyFields= function(target, source, fields, id){
  	var i;
  	if(!id) id = 'id';
  	if(!(source instanceof Array)) return target;
  	if(!(fields instanceof Array)) fields = Object.keys(source);
  	if(target instanceof Array && source instanceof Array){
  		for(i = 0; i < source.length; i++){
  			var sRecord = Util.findBone(source, id, source[i].id);
  			var tRecord = Util.findBone(target, id, source[i].id);
  			if(!tRecord) target.push(tRecord = {id: source[i].id});
  			for(var j = 0; j < fields.length; j++)
  				tRecord[fields[j]] = sRecord[fields[j]];
  		}
  	}
  	else
  		for(i = 0; i < fields.length; i++)
  			target[fields[i]] = source[fields[i]];
  	return target;
  };

  Util.copyFieldsCheckedOnly= function(target, source, fields, id){
  	var i;
  	if(!id) id = 'id';
  	if(!(source instanceof Array)) return target;
  	if(!(fields instanceof Array)) fields = Object.keys(source);
  	if(target instanceof Array && source instanceof Array){
  		for(i = 0; i < source.length; i++){
  			var sRecord = Util.findBone(source, id, source[i].id);
  			var tRecord = Util.findBone(target, id, source[i].id);
  			if(!tRecord && sRecord.check) target.push(tRecord = {id: source[i].id});
  			for(var j = 0; j < fields.length; j++)
  				tRecord[fields[j]] = sRecord[fields[j]];
  		}
  	}
  	else
  		for(i = 0; i < fields.length; i++)
  			target[fields[i]] = source[fields[i]];
  	return target;
  };

  Util.removeDuplicates = function(arr){
  	if(!(arr instanceof Array)) return arr;
  	var second;
  	for(var i=0;i<arr.length;i++)
  		while((second = arr.indexOf(arr[i],arr.indexOf(arr[i]) + 1)) > 0)
  			arr.splice(second,1);
  };

  Util.genRecordSet = function(arr, field){
  	var recs = [];
  	for(var i=0;i<arr.length;i++){
  		var rec = {id: 1+i};
  		rec[field || 'name'] = arr[i];
  		recs.push(rec);
  	}
  	return recs;
  };

  Util.fixContext = function(func, context) {
  	return function() {
  		return func.apply(context, arguments);
  	};
  };

  Util.fixContexts = function(object) {
  	for (var func in object)
  		if (typeof (object[func]) === 'function')
  			object[func] = Util.fixContext(object[func], object);
  	return object;
  };

  Util.lookup = function(obj, field) {
  	if (!obj) { return null; }
  	var chain = field.split(']').join('').split('[');
  	for (var i = 0, len = chain.length; i < len; i++) {
  		var prop = obj[chain[i]];
  		if (typeof(prop) === 'undefined') { return null; }
  		if (typeof(prop) !== 'object') { return prop; }
  		obj = prop;
  	}
  	return null;
  };

  Util.numberWithCommas = function(x) {
  	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  Util.model2columns = function(model){
  	if(!model) return [];
  	var keys = Object.keys(model);
  	var columns = [];
  	if(!(keys instanceof Array) || !keys || !model[keys[0]] || !model[keys[0]].field) return [];
  	for(var i=0;i<keys.length;i++)
  		columns.push(Object.assign( {id: keys[i]} , model[ keys[i] ] ));
  	return columns;
  };

  Util.jsonToArray = function(jsonArray){
  	var s = [];
  	try {
  		s = JSON.parse(jsonArray);
  	} catch(e) {
  		s = [];
  	}
  	return s;
  };

  Util.arrayToJson = function(array, clearHashKey){
  	var s = '[]';
  	if(!(array instanceof Array)) return s;
  	if(clearHashKey)
  		Util.clearField(array, '$$hashKey');
  	try {
  		s = JSON.stringify(array);
  	} catch(e) {
  		s = '[]';
  	}
  	return s;
  };

  Util.jsonToObject = function(jsonObject){
  	var s = {};
  	try {
  		s = JSON.parse(jsonObject);
  	} catch(e) {
  		s = {};
  	}
  	return s;
  };

  Util.objectToJson = function(object, pretty){
  	var s = '{}';
  	if(!(object instanceof Object)) return s;
  	try {
  		s = JSON.stringify(object, pretty?null:undefined, pretty?2:undefined);
  	} catch(e) {
  		s = '{}';
  	}
  	return s;
  };

  Util.jsonToAny = function(jsonObject){
  	var s = null;
  	try {
  		s = JSON.parse(jsonObject);
  	} catch(e) {
  		s = null;
  	}
  	return s;
  };

  Util.anyToJson = function(object){
  	var s = 'null';
  	try {
  		s = JSON.stringify(object);
  	} catch(e) {
  		s = 'null';
  	}
  	return s;
  };

  Util.hex2str = hex=>{
      var str = '';
      for (var i = 0; i < hex.length; i += 2) {
          var v = parseInt(hex.substr(i, 2), 16);
          str += Util.chra(v % 256);// += chr(v % 256);
      }
      return str;
  };

  Util.hash = str=>{ // btoa not encode as encoding bytes not 2byte UTF16 Characters.
  	return Base64$1.btoa(Util.hex2str(sha256(str)));
  };

  Util.chra = ch=>(
  	'\x00'+'\x01'+'\x02'+'\x03'+'\x04'+'\x05'+'\x06'+'\x07'+'\x08'+'\x09'+'\x0A'+'\x0B'+'\x0C'+'\x0D'+'\x0E'+'\x0F'+
  	'\x10'+'\x11'+'\x12'+'\x13'+'\x14'+'\x15'+'\x16'+'\x17'+'\x18'+'\x19'+'\x1A'+'\x1B'+'\x1C'+'\x1D'+'\x1E'+'\x1F'+
  	'\x20'+'\x21'+'\x22'+'\x23'+'\x24'+'\x25'+'\x26'+'\x27'+'\x28'+'\x29'+'\x2A'+'\x2B'+'\x2C'+'\x2D'+'\x2E'+'\x2F'+
  	'\x30'+'\x31'+'\x32'+'\x33'+'\x34'+'\x35'+'\x36'+'\x37'+'\x38'+'\x39'+'\x3A'+'\x3B'+'\x3C'+'\x3D'+'\x3E'+'\x3F'+
  	'\x40'+'\x41'+'\x42'+'\x43'+'\x44'+'\x45'+'\x46'+'\x47'+'\x48'+'\x49'+'\x4A'+'\x4B'+'\x4C'+'\x4D'+'\x4E'+'\x4F'+
  	'\x50'+'\x51'+'\x52'+'\x53'+'\x54'+'\x55'+'\x56'+'\x57'+'\x58'+'\x59'+'\x5A'+'\x5B'+'\x5C'+'\x5D'+'\x5E'+'\x5F'+
  	'\x60'+'\x61'+'\x62'+'\x63'+'\x64'+'\x65'+'\x66'+'\x67'+'\x68'+'\x69'+'\x6A'+'\x6B'+'\x6C'+'\x6D'+'\x6E'+'\x6F'+
  	'\x70'+'\x71'+'\x72'+'\x73'+'\x74'+'\x75'+'\x76'+'\x77'+'\x78'+'\x79'+'\x7A'+'\x7B'+'\x7C'+'\x7D'+'\x7E'+'\x7F'+
  	'\x80'+'\x81'+'\x82'+'\x83'+'\x84'+'\x85'+'\x86'+'\x87'+'\x88'+'\x89'+'\x8A'+'\x8B'+'\x8C'+'\x8D'+'\x8E'+'\x8F'+
  	'\x90'+'\x91'+'\x92'+'\x93'+'\x94'+'\x95'+'\x96'+'\x97'+'\x98'+'\x99'+'\x9A'+'\x9B'+'\x9C'+'\x9D'+'\x9E'+'\x9F'+
  	'\xA0'+'\xA1'+'\xA2'+'\xA3'+'\xA4'+'\xA5'+'\xA6'+'\xA7'+'\xA8'+'\xA9'+'\xAA'+'\xAB'+'\xAC'+'\xAD'+'\xAE'+'\xAF'+
  	'\xB0'+'\xB1'+'\xB2'+'\xB3'+'\xB4'+'\xB5'+'\xB6'+'\xB7'+'\xB8'+'\xB9'+'\xBA'+'\xBB'+'\xBC'+'\xBD'+'\xBE'+'\xBF'+
  	'\xC0'+'\xC1'+'\xC2'+'\xC3'+'\xC4'+'\xC5'+'\xC6'+'\xC7'+'\xC8'+'\xC9'+'\xCA'+'\xCB'+'\xCC'+'\xCD'+'\xCE'+'\xCF'+
  	'\xD0'+'\xD1'+'\xD2'+'\xD3'+'\xD4'+'\xD5'+'\xD6'+'\xD7'+'\xD8'+'\xD9'+'\xDA'+'\xDB'+'\xDC'+'\xDD'+'\xDE'+'\xDF'+
  	'\xE0'+'\xE1'+'\xE2'+'\xE3'+'\xE4'+'\xE5'+'\xE6'+'\xE7'+'\xE8'+'\xE9'+'\xEA'+'\xEB'+'\xEC'+'\xED'+'\xEE'+'\xEF'+
  	'\xF0'+'\xF1'+'\xF2'+'\xF3'+'\xF4'+'\xF5'+'\xF6'+'\xF7'+'\xF8'+'\xF9'+'\xFA'+'\xFB'+'\xFC'+'\xFD'+'\xFE'+'\xFF'
  )[ch];

  Util.stripSlashes = function(str) {
  	return (str + '').replace(/\\(.?)/g, function (s, n1) {
  		switch (n1) {
  		case '\\':
  			return '\\';
  		case '0':
  			return '\u0000';
  		case '':
  			return '';
  		default:
  			return n1;
  		}
  	});
  };

  /* global nhm */

  var id=1;

  const Events = nhm.Events = {
  	on: (type, filters, cb)=>{
  		if(!ons[type]) ons[type] = [];
  		ons[type].push({id, filters, cb});
  		return id++;
  	},
  	// eslint-disable-next-line
  	emit: (type, msg)=>{},
  	off: (type, id)=>{
  		if(!ons[type]) ons[type] = ons[type].filter(on=>on.id!=id);
  	},
  };

  if(nhm.Events==null) nhm.Events = {};
  const ons = Events.ons = nhm.Events.ons || {test:[
  	{
  		id: id++,
  		cb: (message)=>{
  			// eslint-disable-next-line
  			nhm.Log.log('Test event: "'+message+'"');
  		}
  	},
  ]}; // record of handlers by message type

  const _handler = function(event){
  	if(ons[event.type]) ons[event.type]
  		.forEach(on=>on.cb(event.message));
  };

  // eslint-disable-next-line
  const Data$1 = nhm.Data('events');

  var last = moment(nhm.Global.serverTime).subtract(10,'seconds');
  const fetchLoop = ()=>{
  	const config = nhm.Global.config;
  	const matches = [];
  	const filters = [matches,{field: 'createdOn', op: 'GE', value: last.format(config.timeFormat)}];
  	Object.keys(ons).forEach(type=>matches.push({field: 'type',value: type}));
  	const options = {
  		TYPE: 'events',
  		METHOD: config.wait?'wait':'list',
  		data:{ filters }
  	};
  	nhm.Data.fetchData(config.data, options).then(events=>{
  			last = moment();
  			if(events instanceof Array) events.forEach(_handler);
  			setTimeout(fetchLoop,10000);

  	}).catch(err=>{
  			// eslint-disable-next-line
  			nhm.Log.error('Events fetchData error: '+JSON.stringify(err));
  	});
  };

  Events.start = () =>{
  	nhm.Comms.init();
  	nhm.Util.nextTick(fetchLoop);
  };

  /* eslint-disable */

  /* global nhm */
  // import $ from 'jquery'

  var Filters = nhm.Filters = function(TYPE, options){
  	return {
  		gen: (field = 'id', op='EQ', value=0)=>{},
  	};
  };

  Object.assign(Filters,{
  	match: (type, filters, obj)=>{
  		if(filters instanceof Array) return Filters.matchAll(type, filters, obj);
  		if(!(filters instanceof Object)) return false;
  		const field = filters.field;
  		const Type = nhm.Select.types[type];
  		if(!Type || !field || !type.model[field]) return false;
  		//const value = Filters.convertValueToType(filters.value, Type.model[field].type);
  		const value = filters.value;
  		switch((filters.op || 'EQ').toUppercase()){
  			case 'EQ': return obj[field] == value;
  			case 'LT': return obj[field] < value;
  			case 'GT': return obj[field] > value;
  			case 'LE': return obj[field] <= value;
  			case 'GE': return obj[field] >= value;
  			case 'NE': return obj[field] != value;
  		}
  		return false;
  	},
  	matchAll: (type, filters, obj)=>{
  		return filters.filter(filter=>{
  			if(filter instanceof Array)
  				return Filters.matchAny(type, filters, obj);
  			else
  				return Filters.match(type, filter, obj).length == filters.length
  		});
  	},
  	matchAny: (type, filters, obj)=>{
  		return filters.filter(filter=>{
  			if(filter instanceof Array)
  				return Filters.matchAll(type, filters, obj);
  			else
  				return Filters.match(type, filter, obj).length > 0
  		});
  	},
  });

  /* global nhm */

  const Log = nhm.Log = {
    log: function(tag, log){
      const config = nhm.Global.config.log;
      // eslint-disable-next-line
      console.log.apply(null, log);
      return nhm.Data.fetchData(config, {type:'log', tag, log}).catch(e=>{});
    },
    error: function(tag, log){
      const config = nhm.Global.config.log;
      // eslint-disable-next-line
      console.error.apply(null, arguments);
      return nhm.Data.fetchData(config, {type:'error', tag, log}).catch(e=>{});
    },
    dir: function(tag, log){
      const config = nhm.Global.config? nhm.Global.config.log: {};
      // eslint-disable-next-line
      console.dir.apply(null, arguments);
      return nhm.Data.fetchData(config, {type:'dir', tag, log}).catch(e=>{});
    },
  	genLogger: tag => log =>{
      const config = nhm.Global.config? nhm.Global.config.log: {};
  		return nhm.Data.fetchData(config, {
  			logType: 'gen', tag,
  			log: log instanceof Object && log.message? log.message: log != null? log: 'No error data' });
  	},
  	genFailer: tag => err =>{
      const config = nhm.Global.config.log;
  		return nhm.Data.fetchData(config, {
  			logType: 'gen', tag,
  			log: err instanceof Object && err.message? err.message: err != null? err: 'No error data' });
  	}
  };

  /* global window */

  if(!window.nhm) window.nhm = {};
  const nhm$1 = window.fxc = window.nhm;
  nhm$1.version = version;

  var mods = {Global, Data, DAO, User, Comms, Select, Util, Events, Filters, Log};
  Object.keys(mods).forEach((mod)=>{
    if(!window.fxc[mod]) window.fxc[mod] = mods[mod];
    else Object.assign(window.fxc[mod], mods[mod]);
  });

  var index = window.fxc;

  return index;

}(sha256, moment));
